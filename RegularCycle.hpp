//
// Created by diediaz on 23-08-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_REGULARCYCLE_HPP
#define FASTQ_QUAL_COMPRESSION_REGULARCYCLE_HPP

#include "Cycle.hpp"
class RegularCycle : public Cycle {

private:
    sdsl::wt_huff<sdsl::rrr_vector<63>> m_quals_wt;

public:
    explicit RegularCycle(std::vector<unsigned char>& quals_map){
       m_rank2qual = quals_map;
    };

    size_t size_in_bytes() override {
        return sdsl::size_in_bytes(m_quals_wt)+m_rank2qual.size();
    }

    void build_array(std::string& buffer_file) override {
        sdsl::construct(m_quals_wt, buffer_file);
    }

    unsigned char operator[](size_t index) override{
       return m_rank2qual[m_quals_wt[index]-1];
    };

    double size_diff() override {
        double H0=0;
        size_t n_symbols=0;

        for(uint8_t i=1;i<=m_quals_wt.sigma;i++){
            n_symbols=m_quals_wt.rank(m_quals_wt.size()-1, i);
            H0 += double(n_symbols)/m_quals_wt.size() * log2(double(m_quals_wt.size())/n_symbols);
        }
        return sdsl::size_in_bytes(m_quals_wt)-((m_quals_wt.size()*H0)/8);
    }
};


#endif //FASTQ_QUAL_COMPRESSION_REGULARCYCLE_HPP

#FASTQ qualities compressor

A tool for compressing quality scores of Illumina FASTQ files.

##Prerequisites
* SDSL-lite
* CMake 3.10 or greater
* C++11
* GCC or Clang

##Install
```
$git clone https://DiegoDiazDominguez@bitbucket.org/DiegoDiazDominguez/fastqualcompressor.git
$cd fastqualcompressor/
$mkdir build && cd build
$cmake ..
$make
```

Executable will be placed in the bin folder of the repository

##Example
``
$fastq_comp fastq_file.fq 50
``

Where fastq_file.fq is the input data and 50 is the T threshold (threshold for which all the scores above this value are collapsed). 

##Technical notes

In the **external/CompactDataStructures** folder there are implementations of several data structures
described in the book "Comapct Data Structures, a practical approach" from profesor Gonzalo Navarro.

For the moment, this implemented data structures are:

1. FixedWordArray (arrays of fixed-length cells)
2. PBitVector (plain bit vectors)
3. CBitVector (RRR compressed bit vectors)
4. RankSupport (Rank support for plain bit vectors)
5. SelectSupport (Select support for plain bit vectors)
6. SparseSelectSupport (Select support for sparse bit vectors)
7. HuffWaveletTree (Huffman-shaped wavelet Tree)
8. HuffmanCodes (Algorithm to generate Huffman Codes)

##Important

In the paper it is stated that the Huffman-shaped Wavelet Trees of WT cycles are concatenated together in a bitmap and that the 
Huffman-shaped Wavelet Trees of AP are also concatenated together in another bitmap. It is also stated that for every one of those bitmaps there is a rank
dictionary to support rank operations. For reasons of time, this implementation does not stick to that description. Instead,
every cycle has its own Huffman-shaped Wavelet Tree with its own rank data structure. For the moment, the current Wavelet Trees implementation is the
sdsl::wt_huff<rrr_vector<63>> from the SDSL-lite library. I am working to implement the full data structure as described in the paper. 


//
// Created by diediaz on 23-07-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_FASTQUALCOMPRESSOR_HPP
#define FASTQ_QUAL_COMPRESSION_FASTQUALCOMPRESSOR_HPP

#include <iostream>
#include <vector>
#include "FastQReader.hpp"
#include "PBitVector.hpp"
#include "CBitVector.hpp"
#include "RankSupport.hpp"
#include "SelectSupport.hpp"
#include "SparseSelectSupport.hpp"
#include <sdsl/wavelet_trees.hpp>
#include "RegularCycle.hpp"
#include "PartitionedCycle.hpp"

class CompFQualArray{

private:
    std::string m_input_file;
    size_t m_n_quals, n_cycles, n_reads;

    //collapse all the qualities above m_qual_threshold
    unsigned char m_qual_threshold;

    PBitVector cycle_is_partitioned;
    std::vector<Cycle*> cycles;

private:
    void compressQuals();
    void getStatsPerCycle();
    double getAPspace(std::vector<std::pair<unsigned char, size_t>> freqs);
    double getWTspace(std::vector<std::pair<unsigned char, size_t>> freqs);

public:
    explicit CompFQualArray(std::string& file, unsigned char qual_threshold);
    unsigned char decodeValue(size_t index);//access to the original values
    size_t size_in_bytes();
    size_t size();
    inline unsigned char operator[](size_t index){
        assert(index<m_n_quals);
        return decodeValue(index);
    };
};


#endif //FASTQ_QUAL_COMPRESSION_FASTQUALCOMPRESSOR_HPP

//
// Created by diediaz on 23-08-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_CYCLE_HPP
#define FASTQ_QUAL_COMPRESSION_CYCLE_HPP

#include <sdsl/wavelet_trees.hpp>
#include "FixedWordArray.hpp"

class Cycle {

    friend class CompFQualArray;

protected:
    std::vector<unsigned char> m_rank2qual;

public:
virtual ~Cycle() = default;;
virtual size_t size_in_bytes()=0;
virtual double size_diff()=0;
virtual void build_array(std::string& buffer_file){};
virtual unsigned char operator[](size_t index)=0;

};


#endif //FASTQ_QUAL_COMPRESSION_CYCLE_HPP

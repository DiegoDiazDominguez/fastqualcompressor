\documentclass[review]{elsarticle}

\usepackage{lineno,hyperref}
\modulolinenumbers[5]
\usepackage[utf8]{inputenc}
\journal{Compact Data Structures Course}
\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\usepackage{booktabs}
\usepackage{graphicx}
%%%%%%%%%%%%%%%%%%%%%%%
%% Elsevier bibliography styles
%%%%%%%%%%%%%%%%%%%%%%%
%% To change the style, put a % in front of the second line of the current style and
%% remove the % from the second line of the style you would like to use.
%%%%%%%%%%%%%%%%%%%%%%%

%% Numbered
%\bibliographystyle{model1-num-names}

%% Numbered without titles
%\bibliographystyle{model1a-num-names}

%% Harvard
%\bibliographystyle{model2-names.bst}\biboptions{authoryear}

%% Vancouver numbered
%\usepackage{numcompress}\bibliographystyle{model3-num-names}

%% Vancouver name/year
%\usepackage{numcompress}\bibliographystyle{model4-names}\biboptions{authoryear}

%% APA style
%\bibliographystyle{model5-names}\biboptions{authoryear}

%% AMA style
%\usepackage{numcompress}\bibliographystyle{model6-num-names}

%% `Elsevier LaTeX' style
\bibliographystyle{elsarticle-num}
%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{frontmatter}

\title{Compressing Illumina quality scores using Wavelet Trees and Alphabet Partitioning}
%\tnotetext[mytitlenote]{Fully documented templates are available in the elsarticle package on \href{http://www.ctan.org/tex-archive/macros/latex/contrib/elsarticle}{CTAN}.}

%% Group authors per affiliation:
\author[]{Diego Díaz Domínguez}%\fnref{myfootnote}}
\address{Department of Computer Science, University of Chile}
%\fntext[myfootnote]{Since 1880.}

%% or include affiliations in footnotes:
%\author[mymainaddress,mysecondaryaddress]{Elsevier Inc}
%\ead[url]{www.elsevier.com}

%\author[mysecondaryaddress]{Global Customer Service\corref{mycorrespondingauthor}}
%\cortext[mycorrespondingauthor]{Corresponding author}
%\ead{support@elsevier.com}

%\address[mymainaddress]{Beauchef 851, Santiago}
%\address[mysecondaryaddress]{360 Park Avenue South, New York}

\begin{abstract}

Quality strings represent in \texttt{Phred} score the probabilities of the nucleotides of being correctly inferred from DNA during sequencing. Encoding quality symbols is more difficult than DNA symbols because their entropy is usually higher. For Illumina sequencing, a.k.a, parallel sequencing, the overall entropy is often higher compared to the entropy of particular sequencing cycles. In this work, we propose a straightforward lossless compression scheme (with an optional lossy parameter) that differentially encodes the sequencing cycles according to their empirical zero-order entropy by using a Huffman-shaped Wavelet Tree or using a technique for variable-length codes based on alphabet partitioning. Results show that we achieve no just good compression rates (30\% of the original size on average using the lossy parameter) but also fast access time to retrieve the original symbols.

\end{abstract}

\begin{keyword}
Bioinformatics \sep DNA sequencing\sep Illumina \sep Statistical compression
%\MSC[2010] 00-01\sep  99-00
\end{keyword}

\end{frontmatter}

%\linenumbers

\section{Introduction}

DNA sequencing is the process of inferring the sequence of nucleotides from DNA molecules. Briefly, a DNA sample is broken into millions of shorter and overlapping fragments, all of them of the same length. Then, a machine (the sequencer) receives the sample and emits symbols in the alphabet $\Sigma=\{A, G, C, T\}$ that represent the predicted nucleotides in the fragments. Every symbol that the machine emits is paired with a quality score that encodes in \texttt{Phred} scale the probability of error of the base call (i.e., the inference of the nucleotide). A quality of $Q$ in \texttt{Phred} format means a $10^{Q/10}$ error probability.

Most of the sequencers start to encode qualities from symbol \texttt{!=33} (quality score=0), and the highest score is usually \texttt{K=75} (quality score=42). As sequencing technologies have improved over the years, quality scores had become increasingly high. This improvement is especially significant for platforms like Illumina\footnote{https://www.illumina.com}, but not for third generation sequencing technologies, such as PacBio\footnote{https://www.pacb.com} or Oxford Nanopore\footnote{https://nanoporetech.com}.

Qualities are an essential piece of data for many types of bioinformatic analysis. For instance, in read alignment, they are used to generate mapping quality scores, or in SNP calling they are used as input for statistical inference.

Sequencing capacities are getting bigger every year (reaching even several TB of space), and representing quality scores succinctly for long-term storage, network transmission or space-efficient analysis has become a challenge. Unlike DNA, the entropy of quality strings is frequently high as they reflect the level of noise. Therefore, using methods that exploit unbalanced symbols frequencies or repetitions to compress the data does not always guarantee good results.  

Among all the sequencing technologies, Illumina is the most popular, mainly because the high volumes of data it can produce, its affordable price and the high quality of its reads. Illumina's technology is frequently referred to as parallel sequencing. This name derives from the fact that nucleotides are drawn in parallel in consecutive steps called cycles. In simple terms, this means that all the symbols at position one in the DNA fragments are called together during the first cycle; consecutively, all the symbols at position two are drawn together in the second cycle and so forth. The number of cycles defines the size of the sequencing reads\footnote{ string generated by a sequencer, and that represents a DNA fragment} in the set.

The amount of error can vary between cycles. This fluctuation in noise can be due to several factors; human manipulation, protocol calibration, machine specifications, environmental factors, among other things. It is usual, for instance, that at a given cycle $i$, most of the quality scores lie in a very narrow range, say \texttt{35}-\texttt{40}, but in the next cycle $i+1$, they distribute on a broader range, \texttt{15}-\texttt{35}. The opposite also might happen, a change from a wide quality distribution to a tight one (see example in figure \ref{fig:cycle_examples}). It follows from this that the $H_{0}$ entropy will be low in those cycles where the qualities are restricted to a small range and higher in those cycles where the qualities distribute in wider ranges. If the sequencing process was irregular, then there will be a high fluctuation in noise among the cycles. Thus, the total entropy $H_{0}$  of the set will be higher compared to per-cycle $H_{0}$ entropies.

\section{Our contribution}

In this work, we propose a new lossless compression scheme (with optional lossy extra compression) designed for Illumina quality scores. This scheme is built on top of two well-known data structures used for statistical compression, Huffman-shaped Wavelet Trees and Alphabet Partitioning. Briefly, our data structure differentially encodes the qualities according to the empirical zero-order entropy of the cycles. This structure uses has a $O(\log u)$ worst-case time complexity for accessing the elements, where $u$ is the maximum number of quality symbols seen in any cycle.

\section{Preliminaries}
\subsection{Wavelet trees}

The Wavelet Tree \cite{Grossi:2003:HET:644108.644250} is a data structure that encodes a sequence $S=[1..n]$ of symbols over an alphabet $\Sigma=[1..\sigma]$ using $n\log\sigma + o(n\log\sigma) + O(w\sigma)$ bits and supports the operations $rank$, $select$ and $access$ in $O(\log\sigma)$ time. Using Wavelet Trees is convenient when the alphabet is small or moderate, as in theses cases it needs near optimal $n\log\sigma$ bits. 

The Wavelet Tree uses a binary tree structure to partition the alphabet hierarchically. This partition is propagated recursively over the nodes of the tree until reaching the leaves, that stores the symbols of the alphabet. To build the Wavelet Tree, we proceed as follows:  we split the alphabet in two equal parts $l=[1..\ceil*{\sigma/2}]$ and $r=[\ceil*{\sigma/2}+1..\sigma]$. All the symbols within the range of $l$ will belong to the left child of the root, and all the symbols within the range of $r$ will belong to the right child. Then, we scan $S$ and in doing so we fill a bitmap $B=[1..n]$ with $B[i]=0$ if $S[i] \in l$ or $B[i]=0$ if $S[i] \in r$. During the scan we also fill other two arrays $S_l \in l^{+}$ and $S_r \in r^{+}$, where $S_l$ (respectively $S_r$) contains the concatenated symbols of $S$ that belong to $l$ (respectively $r$) in the same order as they appear in $S$. Finally, we discard $S$, store $B$ in the root, and we use $S_l$ and $S_r$ to recursively build the left and right nodes until we finish the tree. Notice that every level of the tree has $n$ bits, and the total depth of the tree is $\log \sigma$.

The operations $rank$, $access$ and $select$ in the Wavelet Tree of $S$ are calculated by performing successive $rank$ or $select$ queries over the bitmaps of the nodes while descending through the tree. There are several data structures proposed in the literature for $rank$ and $select$ operations on bitmaps \cite{gonzalez2005practical,okanohara2007practical,Vigna:2008:BIR:1788888.1788900,gog2014optimized,clark1997compact} that work fast in practice. Some of them even work in constant time while using $o(n)$ bits of space, where $n$ is the size of the bitmap.

\subsubsection{Huffman-shaped Wavelet Tree}
We can modify the Wavelet Trees to compress $S$ to nearly its empirical zero-order entropy $H_0(S)$ without sacrificing time complexity for $rank$, $select$ or $access$. M\"{a}kinen and Navarro \cite{makinen2005succinct} proposed to replace the shape of the tree with that obtained by generating Huffman codes for the symbols in $S$. To build this Huffman-shaped structure, we first calculate the Huffman code $h(s)$ of every symbol $s$ in $S$. Then, instead of partitioning the alphabet, we assign to the left child those symbols with $h(s)[d]=0$, where $d$ is the depth of the current tree node. Similarly, those symbols with $h(s)[d]=1$ are assigned to the right child. As before, we create a bit vector $B=[1..n]$ to indicate if the symbols belong to the left or right child and we store $B$ in the current node. We apply the same idea recursively over the tree until reaching the leaves, which (again) represent the symbols in $\Sigma$. To answer $rank$, $select$ and $access$ we proceed as before, performing $rank$ and $select$ over the bit vectors of the nodes. 

Unlike the original Wavelet Trees, Huffman-shaped Wavelet Trees are unbalanced, that means that some levels of the tree have less than $n$ bits (especially the deeper ones). Actually, every $h(s)$ will spent one bit per level just until depth $|h(s)|$. Thus, the total space usage of the data structure is $\Sigma_{i=1}^\sigma n_{i}h(i)< n(H_0(S)+1)$ bits, where $n_i$ is the number of occurrences of symbol $i$ in $S$. If we add the $rank$ and $select$ data structures and the pointers to the nodes of the tree, then the space becomes $n(H_0(S)+1)(1+o(1)) + O(w\sigma)$ bits.


\subsection{Alphabet partitioning}\label{AP}

Alphabet partitioning \cite{barbay2014efficient} is a technique to encode sequences that achieves $O(\log \log \sigma)$ time complexity for $rank$, $select$ and $access$ operations while maintaining the space complexity in $nH_{0}(S) + o(nH_{0}(S)) + O(n)$ bits. This data structure uses more space than Huffman-shaped Wavelet Trees in practice so is convenient only when the size of the alphabet is similar to $n$.

The idea of alphabet partitioning has been applied also to other problems. For example, \cite{kulekci2014enhanced} proposed a similar technique that gives direct access to variable-length codes while using $n\log\log \sigma(1+o(1))$ bits of space. This data structure groups the symbols in $\log \sigma$ different classes. The class of $S[i]$ is defined as $l_i=\floor*{\log S[i]}$ and stored in a vector $K=[1..n]$ over the alphabet $[1..\floor*{\log \sigma}]$. Additionally, for every $l \in [1..\log \sigma]$ an array $A_{l}$ of $l$-bit cells is generated. Each $A_l$ keeps the symbols in $S$ that fall in the range $[2^l..2^{l+1}-1]$ encoded as $s-2^{l}$. Notice that elements of class $l=0$ have only one possible value, so it is not necessary to have an $A_l$ for them. In a nutshell, each element $S[i]$ is encoded by first obtaining its class $l=\floor*{\log S[i]}$, then storing $l$ in $K[i]$ and finally, if $l\neq0$, adding the value to $A_l[rank_l(K, i)]= S[i]-2^l$. Decoding $S[i]$ works in a similar way, first we get its class $l=K[i]$, then we get how many elements of class $l$ are until $K[i]$. This value is calculate by performing $p=rank_l(K, i)$. Finally, the original value is $A_l[p]+2^l$. $K$ can be encoded as a Wavelet Tree so $access$ and $rank$ takes $O(\log \log \sigma)$ time and accessing an element of $A_l$ takes constant time. Thus, the whole time complexity for accessing a value is $O(\log\log\sigma)$. 

The technique for variable-length codes described above is competitive with Elias-Fano codes \cite{elias1974efficient}. If we give Huffman shape to the Wavelet Tree of $K$, then the space complexity outperforms the space used by $\gamma$-codes \cite{elias1975universal}, $\delta$-codes \cite{elias1975universal}, Rice codes \cite{rice1971adaptive} and VByte \cite{williams1999compressing} codes.

\begin{figure}[t]
\centering
\hspace*{-2.5cm}
\begin{subfigure}{0.7\textwidth}
  \centering
  \includegraphics[scale=0.35]{good_quality.png}
  \caption{Good quality scores}
  \label{fig:sub1}
\end{subfigure}%
\begin{subfigure}{0.7\textwidth}
  \centering
  \includegraphics[scale=0.35]{bad_quality.png}
  \caption{Bad quality scores}
  \label{fig:sub2}
\end{subfigure}
\caption{Illumina qualities. Boxplots depict good (left) and bad (right) overall sequencing qualities. The y-axis shows the quality score in \texttt{Phred} scale, and the x-axis represents the sequencing cycles. Per-cycle quality distributions are shown as quarterlies (yellow boxes). The red line in every box is the median of the quality score, and the blue line is the mean quality score. Colors at the back of the plots delimit scores considered as bad (red), regular (yellow) and good (green).}
\label{fig:cycle_examples}
\end{figure}

\section{Methods}

\subsection{Basic notation}

Let $\mathcal{S}=\{S_1 \cdot\cdot\cdot S_r\}$ be a set of $r$ strings over the alphabet $\Sigma=[1..50]$ that represent the quality scores of the reads generated during a sequencing experiment. All $S_i \in \mathcal{S}$ have the same size $c$ that corresponds to the number of sequencing cycles. A cycle $C_i$ with $i \in [1..c]$ is defined as the concatenation of the $r$ symbols that appear at position $i$ in the strings of $\mathcal{S}$. Additionally, those $C_i$ stored using $WT$ (respectively $AP$) are referred as $WT(C_i)$ (respectively $AP(C_i)$).

\subsection{Encoding quality scores}

We encode the symbols of every $C_i$ deferentially according to their empirical $H_{0}(C_i)$ entropy. If the entropy of $C_i$ is low, then we use a Huffman-shaped Wavelet tree. On the other hand, if the entropy is high, then we encode $C_i$ using the technique for variable-length codes based on alphabet partitioning described in section \ref{AP}.

From now on, we will refer to the coding scheme based on Huffman-shaped Wavelet trees as WT and the coding scheme based on alphabet partitioning as AP.

For every $C_i$ we proceed as follows; we generate a set $F_i$ that contains tuples with the symbols in $C_i$ paired with their respective frequencies, and sorted in decreasing order according the frequencies. Then, we check which encoding scheme uses less space for the cycle. To decide this, we calculate two values, $S_{WT}$ and $S_{AP}$, that stand for the estimated space usage of $WT(C_i)$ and $AP(C_i)$ respectively. As the space of WT is close to the empirical zero-order entropy of $C_i$, we just use $S_{WT}=rH_{0}(C_i)$. For $S_{AP}$, we first obtain the symbol frequencies of vector $K_i$, that is, the frequencies of the symbol classes in $C_i$. The frequencies of the classes can be obtained with one scan over $F_i$ and filling another vector $A_i$ with the formula $A_i[\floor*{\log(F_i[j].symbol)}+1]=A_i[\floor*{\log(F_i[j].symbol)}+1]+F_i[j].freq$. The vector $A_i$ is then used to calculate $H_{0}(K_i)$. Finally, we obtain $S_{AP}=rH_{0}(K_i) + \Sigma_{j=2}^{|A_i|} (j-1)A_i[j]$. If $S_{WT}\leq S_{AP}$, then we encode the qualities using WT, we use AP otherwise. As we encode the cycles, we also fill a bitmap $P$ of size $c$ where $P[i]=1$ if the \emph{i-th} cycle was encoded using AP and $P[i]=0$ if it was encoded using $WT$. 

\subsection{Reducing the space overhead}\label{spacered}

Notice that we need a Huffman-shaped Wavelet Tree for every cycle due to both WT and AP need one. The $rank$ and $select$ data structures of these Huffman-shaped Wavelet trees might impose a significant space overhead, especially if the number of cycles is big, say, more than 150 cycles. As we only need $access$ and $rank$, we can get rid of the $select$ data structures. However, using plain bit vectors for the nodes of the trees, which is desirable for fast operations, still requires about 25\%-37\% extra space for rank. A possible solution is to use the classical $H_0$ compressed RRR bit vector \cite{pagh2001low}. This data structure uses little space for rank, but operations become slower than in plain bit vectors. We address this issue by using the following scheme: let $p$ be the number of cycles encoded as WT and let $a$ be the number of cycles encoded as $AP$. For every $C_i$ we concatenate all the bit vectors of its Huffman-shaped Wavelet Tree into one super bit vector $B_i$. Now we create a second level of concatenation: all $B_i$ that encodes Huffman-shaped Wavelet Trees of the WT scheme are put together $\mathcal{B}_{WT}=B_j\cdot\cdot B_{j'}$. The same idea is used to create another bit vector $\mathcal{B}_{AP}=B_{k}\cdot\cdot\cdot B_{k'}$. We modify the nodes of the Huffman-shaped trees so that now they store pointers to the positions in $\mathcal{B}_{WT}$ or $\mathcal{B}_{AP}$ where their respective bit vectors start. We also need two arrays $W=[1..p]$ and $L=[1..a]$ to store pointers to the root of every Huffman-shaped Wavelet Tree in the WT and AP scheme respectively. 

We build a rank dictionary for $\mathcal{B}_{AP}$ and for $\mathcal{B}_{WT}$ using a three level scheme as proposed by \cite{okanohara2007practical}. Let $\mathcal{B}$ be a bit vector with the concatenation of several Huffman-shaped Wavelet Trees. In the first level, we obtain the rank answers every $l=2^{16}$ positions of $\mathcal{B}$ and store them in an array $R=[1..\ceil*{n/l}]$. In the second level, we choose a second sampling rate $s=256$ and create an array $R'=[1..\ceil*{n/s}]$ where every $R'[i]$ stores the number of bits set to 1 from position $\mathcal{B}[l\floor*{is/l}]$ to position $\mathcal{B}[is]$. The aim of the $R'$ array is to store the number of bits set to 1 between position $R[i]$ and $R[i+1]$. Notice that every element in $R'$ is at most $2^{16}-s$, so $R'$ can be encoded using a 16-bit cell array. In the third level we repeat the same rational but using a smaller sampling rate $k=64$ and creating an array $R''=[1..n/k]$. In every $R''[i]$ we store the number of bits set to 1 from $\mathcal{B}[s\floor*{ik/s}]$ to $\mathcal{B}[ik]$. Again, the values in $R''$ are at most $s-k=192$ so we can encode $R''$ using a 8-bit cell array. Now, if we want to retrieve the answer until the \emph{j-th} position in $\mathcal{B}$ we just calculate = $R[\floor*{j/l}] + R'[\floor*{j/s}] + R''[\floor*{j/k}] + popcount(\mathcal{B},\floor*{j/k}+1,j)$ 

For answering rank in a particular Wavelet trees of $\mathcal{B}$ we have to do a small tweak in the rank arrays. If during the construction of $R,R'$ or $R''$ we reach a segment in $\mathcal{B}$ that represents a new node, then we have to reset the rank. Thus, if, for example, we are building the \emph{i-th} cell of $R$ and we reach the position $\mathcal{B}[j]$ with $il<j<(i+1)l$ which represents the start position of a new Huffman tree node, then $R[i+1]$ stores the number of bits set to 1 from $\mathcal{B}[j]$ to $\mathcal{B}[(i+1)l]$. In this way, the time complexity for computing rank in any Huffman-Shaped Wavelet Tree of $\mathcal{B}$ is $O(k)$ (time for popcounting). This value is almost constant in practice as most modern computer architectures perform fast $popcount()$ operations in machine words of size $w=64$ or $w=32$. On the other hand, if we use $w=64$, then the total space usage of $\mathcal{B}$ plus its rank structures becomes $n + 64n/2^{16} + 16n/256 + 8n/64 = 1.189n$ bits, where $n$ is the size of $\mathcal{B}$.


\subsection{Accessing the quality scores}

To retrieve the qualities of $\mathcal{S}$ we define the query $access(\mathcal{S},i,j)$, where $i$ stands for the \emph{i-th} read of $\mathcal{S}$ and $j$ is the \emph{j-th} quality symbol of read $i$. Answering $access$ is straightforward. First, we check $P[j]$ to see if cycle $j$ was encoded using AP or WT. If cycle $j$ was encoded using WT, then we perform access in the Wavelet tree of cycle $j$, and then we return the symbol. To access the Huffman-shaped Wavelet Tree of $C_j$ we have to know the start position of its root in $\mathcal{B}_{WT}$. The position of the root is calculated as $root = W[j-rank(P,j)]$. As we are using Huffman-shaped Wavelet Trees, the worst-case time for access is $O(\log \sigma)$, but in practice, it is faster as most common symbols have shorter Huffman codes. On the other hand, if cycle $j$ was encoded using AP, then we first access to element $i$ in the Wavelet Tree $l=K_j[i]$ to obtain the class $l$ of the query symbol in cycle $j$. Similarly to the WT scheme, we have to know the position of the root node of $K_j$ in $\mathcal{B}_{AP}$; this position can be retrieved with $root=L[rank(P,j)]$. To retrieve the original value $\mathcal{S}[i][j]$ we also need the rank $r$ of class $l$ until position $i$ in $K_j$. As rank is also a subroutine of access in the Wavelet Tree, we modify access to return the pair $(l, r)$. If $l$ is 0, then we return $F_j[0].symbol$. If $l\geq1$, then we obtain the value $u=A_{l}^{j}[r]$, calculate $s=2^{l}+u$ and finally return $F_j[s].symbol$. Besides accessing an element of $K_j$, which has worst-case access time $O(\log\log \sigma)$, all other operations take constant time, so the total time complexity for decoding a symbol quality of a cycle encoded as AP is $O(\log\log \sigma)$.

\subsection{Space complexity}\label{space}

For WT we have that the space of any Huffman-shaped Wavelet Tree is upper bounded by $r(H_{0}(C_i)+1)$. Therefore, the total space of $\mathcal{B}_{WT}$ is at most $\Sigma_{i=1}^{p} r(H_{0}(C_i)+1)= r((\Sigma_{i=1}^{p}H_0(C_i)) + p)$ bits. Adding the rank data structure described in \ref{spacered} we achieve $r((\Sigma_{i=1}^{p}H_0(C_i)) + p)(1+o(1))$ bits of space. Following a similar reasoning, we have that $\mathcal{B}_{AP}$ (including its rank structure) uses $r((\sum_{i=1}^{a}H_0(K_i)) + a)(1+o(1))$ bits of space. Adding both spaces we have that $\mathcal{B}_{AP}$+$\mathcal{B}_{WT}$ use $r(\sum_{i=1}^{a}H_0(K_i) + \sum_{i=1}^{p}H_0(C_i) + a + p)(1+o(1))+ O(w\sigma + c)$ bits. 

\section{Experiments}

To evaluate the level of compression achieved by our data structure, we simulated different sets of paired-end\footnote{https://www.illumina.com/science/technology/next-generation-sequencing/paired-end-vs-single-read-sequencing.html} sequencing reads from the \emph{Escherichia coli} genome, considering several sources of noise. The simulated reads were generated with the software \texttt{art\_illumina} \cite{huang2011art}, taking into consideration the error profile of eight different Illumina platforms (see table \ref{platforms}). For every platform, we created datasets with low, regular and high error rates. The error rate was controlled with the \texttt{--qShift} option. Using positive values for \texttt{--qShift} reduces the noise levels of the profile. On the other hand, using negative values increases the noise. In general, if shifting scores by $x$, then the error rate will be $1/10^{x/10}$. For the experiments, we used quality shifts ranging from -10 to 10 in all the platforms. Additionally, two sequencing coverages were considered: 5x and 10x. In total, the experimental design yielded 800 (considering paired-end) datasets.

The compression rate was measured as the total number of bytes used by our data structure divided by the total number of quality symbols in the set (assuming that uncompressed strings use one byte per symbol). We also included an optional lossy approach in which all the quality symbols above given threshold $T$ were approximated to $T$. We tested different thresholds ranging from 30 to 40.

Experiments were performed in a machine with \texttt{GNU/Linux 4.9.10 Debian} and kernel \texttt{4.9.0-8}, CPU with 32 cores \texttt{Intel Xeon} and 252 \texttt{GB} of RAM.

The data structure was implemented in \texttt{C++} and is freely available at \url{https://bitbucket.org/DiegoDiazDominguez/fastqualcompressor}.

\begin{table}[]

\centering \resizebox{\textwidth}{!}{\begin{tabular}{lccccc}
\textbf{Illumina Platform} & \textbf{Platform Code} & \textbf{Read length} & \textbf{Coverages} & \textbf{Quality shifts} \\
\toprule
Genome Analyzer I  & GA1  & 2 x 36 bp    & 5x,10x & from -10 to 10\\
Genome Analyzer II & GA2  & 2 x 75 bp    & 5x,10x & from -10 to 10\\
HiSeq 1000         & H10  & 2 x 100 bp   & 5x,10x & from -10 to 10\\
HiSeq 2000         & H20  & 2 x 100 bp  & 5x,10x & from -10 to 10\\
HiSeq 2500         & H25  & 2 x 150 bp   & 5x,10x & from -10 to 10\\
HiSeqX PCR free    & HSXn & 2 x 150 bp  & 5x,10x & from -10 to 10\\
HiSeqX TruSeq      & HSXt & 2 x 150 bp  & 5x,10x & from -10 to 10\\
NextSeq500 v2      & NS50 & 2 x 75 bp   & 5x,10x & from -10 to 10\\
\bottomrule
\end{tabular}}
\caption{Description of the Illumina platforms and the datasets generated for each one of them.}
\label{platforms}
\end{table}

\section{Results}

The compression rates of all the datasets are shown in figure \ref{res}. The maximum compression level was achieved in HSXt datasets, using just 4,5\% of the original dataset space. On the other hand, the worst case was for GA2 datasets, with 62,1\% of the original space. On average, the space usage of our data structure was 28\% of the input dataset.

Quality shifts did not have an impact on the level of compression.

In general, forward reads seems to achieve better compression (first column of plots) than reverse reads (left column plots). This difference might be an effect of the sequencing process. Forward reads are generated first, and as the machine components begin to desynchronize as sequencing progresses, reverse reads are more prone to error.

The effect of using a threshold of $T$ for lossy compression had a more notorious effect in older Illumina platforms (GA1, GA2, HS10, HS20, and H25). This behavior is expected as more recent machines produce higher qualities.

The average time for accessing the qualities was between 1 and 2 $\mu$seconds in all datasets. 

\section{Conclusion and further work}

We achieved reasonable levels of compression for all the studied Illumina platforms, being better for the most recent ones. Besides, we maintain good access time to query the quality scores in all the cases. We believe that our index is not just useful for permanent storage, but also for bioinformatic analyses. In the future, we have plans to include it as part of a data structure for indexing sequencing reads. 

We also will explore other encoding schemes based on Run-length techniques or Lempel-Ziv parsing to improve compression further. The data structure we are proposing can be seen as a vertical compressor because it exploits column-wise patterns (sequencing cycles) in the set. However, contiguous positions within the reads also tend to be similar. The ideal data structure for quality scores is one that exploits patterns in both columns and rows to maximize compression.  

Finally, we shall study the possibility of extending our index to third-generation sequencing datasets, such as PacBio or Oxford Nanopore. This goal is more challenging as quality symbols are very noisy compared to Illumina platforms.

\begin{figure}[t]
\centering
\includegraphics[width=\textwidth]{Rplot.pdf}
\caption{Results of the compression rates obtained during the experiments. The compression rate (y-axis) was measured as the total size of the data structure divided by the total number of quality symbols of the set. The x-axis represents the different Illumina platforms codes as shown in table \ref{platforms}. The first column of plots indicates the forward reads and the second column indicates the reverse reads. Plots in rows represent the different quality shifts from -4 to 4. Colors indicate the value used for the quality threshold $T$, where light blues are values close to 30 and dark blues are closer to 40.}
\label{res}
\end{figure}

\section*{References}

\bibliography{ref.bib}

\end{document}
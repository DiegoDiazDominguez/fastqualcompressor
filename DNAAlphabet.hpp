//
// Created by Diego Diaz on 3/23/18.
//

#ifndef OMNITIGSUNIBOSS_DNA_ALPHABET_HPP
#define OMNITIGSUNIBOSS_DNA_ALPHABET_HPP

#include<iostream>

class DNAAlphabet {

public:
    //typedef vector<>::size_type         size_type;
    //typedef size_type[6]              C_type;
    typedef uint8_t                     symbol_type;

    static const size_t                sigma;
    static const unsigned char         comp2char[6];
    static const unsigned char         comp2rev[6];
    static const unsigned char         char2comp[117];

    //const & C =                   m_C;

private:
    //size_type[] m_C = {0, 0, 0, 0, 0, 0, 0};

public:
    DNAAlphabet() = default;;
    //explicit DNAAlphabet(C_type& char_freqs):m_C(char_freqs){}
    //size_type serialize(std::ostream& out, structure_tree_node* v=nullptr, std::string name="")const;
    //void load(std::istream& in);
    //void swap(DNAAlphabet& in);
    //symbol_type rank2char(size_t rank)const;
};


#endif //OMNITIGSUNIBOSS_DNA_ALPHABET_HPP

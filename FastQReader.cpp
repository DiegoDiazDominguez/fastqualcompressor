//
// Created by Diego Diaz on 6/1/18.
//

#include "FastQReader.hpp"

FastQReader::FastQReader(std::string &file, bool dir) {
    m_input_file = file;
    m_direction = dir;
    getFileStats();
    initReader();
}

void FastQReader::getFileStats() {
    kseq_t *seq;
    int l;
    gzFile fp = gzopen(m_input_file.c_str(), "r");
    seq = kseq_init(fp);
    m_n_reads=0;
    m_n_chars=0;
    while ((l = kseq_read(seq)) >= 0) {
        m_n_reads++;
        m_n_chars+=l;
        for(size_t i=0;i<l;i++){
            if(seq->qual.s[i]<64){
                PHRED33=true;
            }
        }
    }
    kseq_destroy(seq);
    gzclose(fp);
}

void FastQReader::destroy() {
    kseq_destroy(seq); //  destroy seq
    gzclose(fp); // close the file handler
}

void FastQReader::initReader() {
    m_curr_read_len=0;
    m_curr_read=-1;
    m_printed_chars=0;

    if(seq!=nullptr){
        destroy();
    }

    if(!input_file->empty()){
        fp = gzopen(input_file->c_str(), "r");
        seq = kseq_init(fp);
        nextRead();
    }else{
        printf("Input file is empty\n");
        exit(EXIT_FAILURE);
    }
}


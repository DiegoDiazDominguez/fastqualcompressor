#include <iostream>
#include "CompFQualArray.hpp"
extern "C"{
#include "kseq.h"
#include <zlib.h>
#include <cstdio>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
};
#include <ctime>
#include <sys/resource.h>
#include <iostream>
#include <random>
#include "CBitVector.hpp"


int main(int argc, char *argv[]) {

    std::string fq_file(argv[1]);
    if(argc<3){
        std::cout<<"missing argument"<<std::endl;
        exit(EXIT_FAILURE);
    }
    //construction time (seconds)
    CompFQualArray qc(fq_file, uint8_t(atoi(argv[2])));
    double elapsed_secs=0;
    for(size_t i=0;i<500;i++){
        clock_t begin = clock();
        qc[i];
        clock_t end = clock();
        elapsed_secs += double(end - begin) / CLOCKS_PER_SEC;
    }

    std::cout<<"Input file: "<<fq_file<<std::endl;
    std::cout<<"Lossy threshold T: "<<argv[2]<<std::endl;
    std::cout<<"Size in bytes of the compressed DT: "<<qc.size_in_bytes()<<std::endl;
    std::cout<<"Number of quality symbols in the original input file: "<<qc.size()<<std::endl;
    std::cout<<"Fraction of space of the original qual symbols used by the structure: "<<double(qc.size_in_bytes())/qc.size()<<std::endl;
    std::cout<<"Mean access time: "<<elapsed_secs/500<<std::endl;
    return 0;
}
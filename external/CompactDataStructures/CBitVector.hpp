//
// Created by diediaz on 07-08-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_CBITVECTOR_HPP
#define FASTQ_QUAL_COMPRESSION_CBITVECTOR_HPP
#include <iostream>
#include "FixedWordArray.hpp"
#include "PBitVector.hpp"

//RRR bit vector as explained in profesor's book

template<size_t b=63, size_t k=30>
class CBitVector {

private:
    size_t n;
    FixedWordArray<> C;
    FixedWordArray<> P;
    FixedWordArray<> L;
    PBitVector O;
private:

    std::pair<size_t, size_t> encode(size_t start, size_t end, PBitVector& plain_bv) const{
        size_t c =0, o,j;
        int u, tmp_c;

        for(size_t i=start;i<=end;i++){
            if(plain_bv[i]){
               c++;
            }
        }

        o = 0;
        tmp_c = (int)c;
        j=start;
        u = (int)(b-1);

        while(0<tmp_c<=u){
            if(j<plain_bv.size() && plain_bv[j]==1){
                o += (size_t)binomial_coefficient(u, tmp_c);
                tmp_c--;
            }
            j++;
            u--;
        }
        return std::make_pair(c,o);
    };

    size_t decode(size_t c, size_t o) const {
        size_t B,j,bc;
        B=0;
        j = 1;
        while(c>0){
            bc = (size_t)binomial_coefficient(int(b-j),(int)c);
            if(o>=bc){
                B |= 1ULL<<(b-j);
                o-=bc;
                c--;
            }
            j++;
        }

        return B;
    };

    bool access(size_t index) const {
        size_t i,p,c,o,bkl,c_pos;

        i = index/(b*k);
        c_pos = index/b;
        p = P[i];

        for(size_t j=i*k;j<c_pos;j++){
            p +=L[C[j]];
        }

        c = C[c_pos];
        if(c==0) return false;
        o = O.bitsRead(p,p+L[c]-1);
        bkl = decode(c, o);
        return (bkl & (1ULL<<(b-(index%b)-1)))!=0;
    }

    void buildL(){
        L.setWidth((size_t)ceil(log2(b+1)));
        L.resize(b+1);
        for(size_t i=0;i<=b;i++){
           L[i] = (size_t)ceil(log2(binomial_coefficient((int)b, (int)i)));
        }
    };

    void compressBitVector(const PBitVector &plain_bv){
        size_t n_blocks, n_pointers, o_pos=0, p=0, class_bits;
        std::pair<size_t, size_t> tmp_pair;

        n_blocks = (size_t)ceil((double)plain_bv.size()/b);
        n_pointers = (size_t)ceil((double)plain_bv.size()/(b*k));
        buildL();

        C.setWidth((size_t)ceil(log2(b+1)));
        C.resize(n_blocks);
        //TODO use a better way to initialize O
        O.resize(n_blocks*64);

        P.resize(n_pointers);

        for(size_t i=0;i<n_blocks;i++){

            tmp_pair = encode(i*b, std::min(plain_bv.size()-1, ((i+1)*b)-1), plain_bv);
            if((i%k)==0){
                P[p] = o_pos;
                //std::cout<<P[p]<<std::endl;
                p++;
            }

            class_bits = L[tmp_pair.first];

            o_pos +=class_bits;

            C[i] = tmp_pair.first;
            if(class_bits!=0){
                O.bitsWrite(o_pos - class_bits, o_pos - 1, tmp_pair.second);
            }
        }
        O.resize(o_pos);
        //std::cout<<bitsRead(10,34)<<std::endl;
    }

    double binomial_coefficient(int n, int l) const {
        double p=1;
        if (l>n) return 0;
        if (0 == l || n == l) return 1;
        if (l > (n - l)) l = n - l;
        if (l == n) return n;

        for (size_t i = 1; i <= l; ++i) {
            p *= (n - (l - i));
            p /= i;
        }
        return p;
    }

    void copy(const CBitVector &other){
        //TODO check this.b and other.b are equals
        O = other.O;
        P = other.P;
        L = other.L;
        C = other.C;
        n = other.n;
    }

public:
    explicit CBitVector(const PBitVector &plain_bv){
        assert(b<=64);
        n = plain_bv.size();
        compressBitVector(plain_bv);
    };

    CBitVector(){
        assert(b<=64);
        n=0;
    };

    CBitVector(const CBitVector& other){
        n=0;
        copy(other);
    }

    CBitVector<b,k>& operator =(const CBitVector &other){
        copy(other);
        return *this;
    }

    size_t bitsRead(size_t start, size_t end) const{
        size_t i,p,c,o,bkl,c_start, c_end, n_bits, B, width;
        n_bits = end-start+1;
        assert(n_bits<=64);

        i = start/(b*k);
        p = P[i];

        c_start = start/b;
        c_end = end/b;

        B=0;
        width = end-start;

        for(size_t j=i*k;j<c_start;j++){
            p +=L[C[j]];
        }

        for(size_t j=c_start;j<=c_end;j++){
            c = C[j];

            if(c==0){
                o=0;
            }else {
                o = O.bitsRead(p, p + L[c] - 1);
            }

            p += L[c];
            bkl = decode(c, o);

            for(size_t l= start;l<std::min(end+1, (j+1)*b);l++){
                if((bkl & (1ULL<<(b-(l%b)-1)))!=0){
                    B |= 1ULL<<width;
                }
                width--;
                start++;
            }
        }
        return B;
    }

    void setBitVector(PBitVector &plain_bv){
       if(n!=0){
           O.flush();
           C.flush();
           P.flush();
           L.flush();
       }
       n = plain_bv.size();
       compressBitVector(plain_bv);
    };

    inline size_t size() const{
        return n;
    };

    inline size_t nBits() const{
        size_t acc=0;
        for(size_t i=0;i<C.size();i++){
            acc+=C[i];
        }
        return acc;
    };

    inline size_t size_in_bytes() const{
        return sizeof(size_t)+
               C.size_in_bytes()+
               P.size_in_bytes()+
               L.size_in_bytes()+
               O.size_in_bytes();
    };

    inline bool operator[](size_t index) const{
       assert(index<n);
       return access(index);
    };

    double occupancy() const{
        return double(nBits())/size();
    };

    double h0_emp_entropy(){
        auto m = (double)nBits();
        auto n = (double)size();
        return (m/n)*log2((n/m)) + ((n-m)/n)*log2((n/(n-m)));
    }

    double h1_emp_entropy(){
        size_t z_pos=0, o_pos=0;
        bool bit;
        PBitVector zero(n);
        PBitVector ones(n);
        for(size_t i=1;i<n;i++){
            bit = access(i);
            if(bit){
               ones[o_pos] = access(i-1);
               o_pos++;
            }else{
               zero[z_pos] = access(i-1);
               z_pos++;
            }
        }
        zero.resize(z_pos);
        ones.resize(o_pos);
        return ((double)zero.size()/n) * zero.h0_emp_entropy()+
               ((double)ones.size()/n) * ones.h0_emp_entropy();
    }
};


#endif //FASTQ_QUAL_COMPRESSION_CBITVECTOR_HPP

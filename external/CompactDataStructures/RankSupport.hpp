//
// Created by diediaz on 23-07-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_RANKSUPPORT_HPP
#define FASTQ_QUAL_COMPRESSION_RANKSUPPORT_HPP
#include <iostream>
#include "PBitVector.hpp"
#include "FixedWordArray.hpp"

static const size_t W = (size_t)std::numeric_limits<size_t>::digits;
static const size_t LOG_W = (size_t)log2(W);

//TODO m_k>W is supported just if m_k is a power of two (we can support Rank for any K but I have to modify rank operation)
template<size_t k=W, size_t s=k*4>
class RankSupport {
private:
    friend class SelectSupport;
    const PBitVector *m_bv;
    FixedWordArray<> R;
    FixedWordArray<> r;
    size_t m_k, m_s, words_per_block;

private:
    void populateRankVectors(){
        size_t  R_acc, r_acc, r_size, R_size, pop_count, n_blocks, curr_word;
        n_blocks = m_bv->size()/m_k;

        R_size = m_bv->size()/m_s + 1;
        r_size = n_blocks + 1;

        R.resize(R_size);
        r.resize(r_size);

        R[0]=0;
        r[0]=0;
        r_acc = 0;
        R_acc = 0;

        for(size_t i=1;i<=n_blocks;i++){

            if(m_k>W){
                curr_word = (i-1)*words_per_block;
                pop_count=0;
                for(size_t j=0;j<words_per_block;j++) {
                    pop_count += __builtin_popcountll(m_bv->m_W[curr_word + j]);
                }
            }else{
                pop_count = __builtin_popcountll(m_bv->bitsRead((i - 1) * m_k, i * m_k - 1));
            }

            R_acc+=pop_count;
            r_acc+=pop_count;
            if((i*m_k)%m_s==0){
                r[i] = 0;
                r_acc = 0;
                R[(i*m_k)/m_s] = R_acc;
            }else{
                r[i] = r_acc;
            }
        }
    };
    void copy(const RankSupport &other){
        m_bv = other.m_bv;
        R = other.R;
        r = other.r;
    };

public:
    explicit RankSupport(const PBitVector* bv){
        assert((k <= W || k%W==0) && k<s && (s%k)==0);
        m_k = k;
        m_s = s;
        m_bv = bv;
        words_per_block = m_k/W;
        r.setWidth(1+((size_t)floor(log2(m_s-m_k))));
        populateRankVectors();
    };

    RankSupport(){
        assert((k <= W || k%W==0) && k<s && (s%k)==0);
        m_k = k;
        m_s = s;
        m_bv = nullptr;
        words_per_block = m_k/W;
        r.setWidth(1+((size_t)floor(log2(m_s-m_k))));
        R.resize(0);
        r.resize(0);
    };

    void setSamplingRates(size_t new_k, size_t new_s){
        assert((new_k <= W || new_k%W==0) && new_k<new_s && (new_s%new_k)==0);
        m_k = new_k;
        m_s = new_s;
        words_per_block=m_k/W;
        r.setWidth(1+((size_t)floor(log2(m_s-m_k))));

        if(R.size()!=0 || r.size()!=0){
           R.flush();
           r.flush();
           populateRankVectors();
        }
    }

    void setBitVector(const PBitVector *bv){
        m_bv = bv;
        if(r.size()!=0 || R.size()!=0){
            R.flush();
            r.flush();
        }
        populateRankVectors();
    };

    size_t operator()(size_t pos) const{
        size_t index, start, end, acc=0, n_words;
        assert(pos<m_bv->size());

        /*std::cout<<""<<std::endl;
        for(size_t i=0;i<R.size();i++){
            std::cout<<R[i]<<" ";
        }
        std::cout<<""<<std::endl;
        for(size_t i=0;i<r.size();i++){
            std::cout<<r[i]<<" ";
        }
        std::cout<<""<<std::endl;*/

        index = pos/m_k;
        n_words = (pos%m_k)>>LOG_W;
        start = index*words_per_block;
        end = start + n_words;

        for(size_t i=start;i<end;i++){
            acc += __builtin_popcountll(m_bv->m_W[i]);
        }

        return R[pos/m_s] + r[index] + acc + __builtin_popcountll(m_bv->bitsRead((index*m_k) + (n_words<<LOG_W), pos));;
    };

    inline RankSupport& operator=(const RankSupport& other){
        copy(other);
        return *this;
    };

    size_t size_in_bytes()const{
        return sizeof(size_t)+
               sizeof(m_bv)+
               R.size_in_bytes()+
               r.size_in_bytes();
    };
};



#endif //FASTQ_QUAL_COMPRESSION_RANKSUPPORT_HPP

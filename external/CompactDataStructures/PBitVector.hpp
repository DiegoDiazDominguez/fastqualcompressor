//
// Created by diediaz on 23-07-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_BITVECTOR_HPP
#define FASTQ_QUAL_COMPRESSION_BITVECTOR_HPP
#include <iostream>
#include <cmath>
#include <limits>
#include <cstring>
#include <cassert>

class PBitVector {
    template<size_t k, size_t s>
    friend class RankSupport;
    friend class SelectSupport;

private:
    unsigned long long *m_W;
    const static size_t m_word_size=(size_t)std::numeric_limits<size_t>::digits;
    size_t m_virtual_size;
    size_t m_real_size;
private:
    inline void bitWrite(size_t j, bool bit){
        assert(j<m_virtual_size);
        if(bit){
            m_W[j/m_word_size] |= 1ULL << (j & m_word_size-1);
        }else{
            m_W[j/m_word_size] &= ~(1ULL << (j & m_word_size-1));
        }
    };

    inline bool bitRead(size_t j) const{
        assert(j<m_virtual_size);
        return ((m_W[j/m_word_size] >> (j & (m_word_size-1))) & 1ULL)!=0;
    };

    void copy(const PBitVector& other){
        m_virtual_size = other.m_virtual_size;
        m_real_size = other.m_real_size;
        if(m_real_size!=0){
            if(m_W!= nullptr){
                free(m_W);
            }
            m_W = new unsigned long long int[m_real_size];
            memcpy(m_W, other.m_W, m_real_size*sizeof(unsigned long long int));
        }
    };

    class Proxy{
        friend class PBitVector;
    private:
        PBitVector & m_fwa;
        size_t m_index;
        Proxy(PBitVector & customArray, size_t index): m_fwa(customArray),
                                                           m_index(index){}
    public:
        inline operator bool()const{
            return m_fwa.bitRead(m_index);
        }

        inline Proxy& operator=(bool bit){
            m_fwa.bitWrite(m_index, bit);
            return *this;
        }
    };

public:
    explicit PBitVector(size_t array_size){
        m_virtual_size = array_size;
        m_real_size = (size_t)ceil((double)m_virtual_size/m_word_size);
        m_W = new unsigned long long[m_real_size];
        memset(m_W, 0, m_real_size*sizeof(unsigned long long));
    };

    PBitVector(std::initializer_list<bool> init_list){
        m_virtual_size = init_list.size();
        m_real_size = (size_t)ceil((double)m_virtual_size/m_word_size);
        if(m_real_size!=0){
            m_W = new unsigned long long int[m_real_size];
            memset(m_W, 0, m_real_size*sizeof(unsigned long long));
            std::initializer_list<bool>::const_iterator it = init_list.begin();
            std::initializer_list<bool>::const_iterator end = init_list.end();

            size_t index=0;
            while(it!=end){
                bitWrite(index, *it);
                //std::cout<<W[0]<<" "<<index<<" "<<*it<<std::endl;
                it++;
                index++;
            }
        }
    };

    PBitVector(){
        m_virtual_size = 0;
        m_real_size = 0;
        m_W = nullptr;
    };

    PBitVector(const PBitVector& other){
        m_W = nullptr;
        copy(other);
    };

    ~PBitVector(){
        delete[] m_W;
    };

    void bitsWrite(size_t i, size_t j, size_t value){
        assert(i<=j);
        size_t cell_i, cell_j, n_bits;
        n_bits = j-i+1;
        assert(n_bits <= m_word_size);
        cell_i = i/m_word_size;
        cell_j = j/m_word_size;

        if(cell_i == cell_j){
            if(n_bits==m_word_size){
                m_W[cell_j]=value;
            }else{
                m_W[cell_j] &= ~(((1ULL<<n_bits) -1) << (i & m_word_size-1));
                m_W[cell_j] |= value << (i & m_word_size-1);
            }
        }else{
            m_W[cell_i] = (m_W[cell_i] & ((1ULL<<(i & (m_word_size-1)))-1)) | (value << (i & (m_word_size-1)));
            m_W[cell_j] = (m_W[cell_j] & ~((1ULL<<((j+1) & (m_word_size-1)))-1)) |
                        (value >> (m_word_size-(i & (m_word_size-1))));
        }
    };

    size_t bitsRead(size_t i, size_t j) const{
        assert(i<=j);
        size_t cell_i, cell_j, n_bits;
        n_bits = j-i+1;
        assert(n_bits <= m_word_size);
        cell_i = i/m_word_size;
        cell_j = j/m_word_size;
        if(cell_i == cell_j){
            if(n_bits==m_word_size){
                return m_W[cell_j];
            }else{
                return (m_W[cell_j] >> (i & (m_word_size - 1))) & ((1ULL << n_bits) - 1);
            }
        }else{
            if(n_bits==m_word_size){
                std::cout<<"I don't know if this is possible"<<std::endl;
                exit(EXIT_FAILURE);
            }
            return (m_W[cell_i] >> (i & (m_word_size-1) )  |
                    (m_W[cell_j] & ((1ULL <<((j+1) & (m_word_size-1)))-1)) << (m_word_size - (i & (m_word_size-1))));
        }
    };

    inline size_t size() const{
        return m_virtual_size;
    };

    void resize(size_t new_size){
        m_virtual_size = new_size;
        if(new_size==0){
            m_real_size=0;
            if(m_W!=nullptr){
                delete [] m_W;
            }
            m_W=nullptr;
        }else {
            auto tmp =  (size_t)ceil((double)m_virtual_size/m_word_size);
            if (tmp != m_real_size) {
                auto *tmp_ptr = (unsigned long long *) realloc(m_W, tmp * sizeof(unsigned long long));
                if (tmp_ptr != nullptr) {
                    m_W = tmp_ptr;
                    if (m_real_size == 0) {
                        memset(m_W, 0, tmp * sizeof(unsigned long long));
                    }
                    m_real_size = tmp;
                } else {
                    std::cout << "Error trying to resize the array" << std::endl;
                    exit(EXIT_FAILURE);
                }
            }
        }
    };

    PBitVector& operator=(const PBitVector& other){
        copy(other);
        return *this;
    };

    inline bool operator[](size_t index) const{
        return bitRead(index);
    };

    Proxy operator[](size_t index){
        return Proxy{*this, index};
    };

    inline size_t nBits() const{
        size_t m=0;
        for(size_t i=0;i<m_real_size;i++){
            m += __builtin_popcountll(m_W[i]);
        }
        return m;
    };

    inline size_t popCount(size_t i, size_t j){
        assert(i<=j && i<m_virtual_size && j<m_virtual_size);
        size_t cell_i = i/m_word_size;
        size_t cell_j = j/m_word_size;
        size_t n_bits=0;
        //TODO fix this
        for(size_t k=i;k<=j;k++){
            if(bitRead(k)){
               n_bits++;
            }
        }
        return n_bits;
    }

    size_t size_in_bytes() const{
        return sizeof(m_W)+
               (sizeof(size_t)*3)+
               sizeof(unsigned long long)*m_real_size;
        //TODO Do I have to include Proxy class?
    };

    void flush(){
       memset(m_W, 0, m_real_size*sizeof(unsigned long long));
    }

    double occupancy() const{
        return double(nBits())/size();
    };

    double h0_emp_entropy(){
       auto m = (double)nBits();
       auto n = (double)size();
       return (m/n)*log2((n/m)) + ((n-m)/n)*log2((n/(n-m)));
    }

    double h1_emp_entropy(){
        size_t z_pos=0, o_pos=0;
        bool bit;
        PBitVector zero(m_virtual_size);
        PBitVector ones(m_virtual_size);

        for(size_t i=1;i<m_virtual_size;i++){
            bit = bitRead(i);
            if(bit){
                ones[o_pos] = bitRead(i-1);
                o_pos++;
            }else{
                zero[z_pos] = bitRead(i-1);
                z_pos++;
            }
        }
        zero.resize(z_pos);
        ones.resize(o_pos);
        return ((double)zero.size()/m_virtual_size) * zero.h0_emp_entropy()+
               ((double)ones.size()/m_virtual_size) * ones.h0_emp_entropy();
    }
};


#endif //FASTQ_QUAL_COMPRESSION_BITVECTOR_HPP

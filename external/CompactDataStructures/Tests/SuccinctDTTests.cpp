//
// Created by diediaz on 23-07-18.
//
#include <cstdlib>
#include <gtest/gtest.h>

#include "../PBitVector.hpp"
#include "../FixedWordArray.hpp"
#include "../RankSupport.hpp"
#include "../SelectSupport.hpp"
#include "../SparseSelectSupport.hpp"
#include <random>

template<size_t k, size_t s>
void rankSupportTester(size_t bv_size){

    size_t acc=0;
    bool ran_val;
    PBitVector bv(bv_size);
    auto *rank = new size_t[bv_size];
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<size_t> distr(0, 1);

    for(size_t i=0;i<bv.size();i++){
        ran_val = distr(eng)==1;
        bv[i] = ran_val;
        if(ran_val){
            acc++;
        }
        rank[i] = acc;
    }

    RankSupport<k, s> rs(&bv);
    for(size_t i=0;i<bv.size();i++){
        EXPECT_EQ(rs(i), rank[i]);
    }
    delete[] rank;
}

template<size_t k, size_t s>
void selectSupportTester(size_t bv_size){

    bool ran_val;
    PBitVector bv(bv_size);
    auto *select = new size_t[bv_size];
    auto *rank = new size_t[bv_size];
    size_t tmp=1, n_bits,acc=0;
    select[0] = 0;

    std::random_device rd; // obtain a random number from hardware
    std::mt19937 eng(rd()); // seed the generator
    std::uniform_int_distribution<size_t> distr(0, 1);

    for(size_t i=0;i<bv.size();i++){
        ran_val = distr(eng)==1;
        bv[i] = ran_val;
        if(ran_val){
            select[tmp] = i;
            tmp++;
        }
    }
    n_bits = bv.nBits();

    //fill the rank dictionary
    for(size_t i=0;i<bv.size();i++){
        if(bv[i]){
            acc++;
        }
        rank[i] = acc;
    }

    //test that the Select dictionary was correctly built
    SelectSupport<k, s> ss(&bv);

    /*
    std::cout<<"Bivector length: "<<bv.size()
             <<" | PBitVector size ratio:"<<(double)(bv.size_in_bytes()*8)/bv.size()
             <<" | SelectSupport size ratio:"<<(double)(ss.size_in_bytes()*8)/bv.size()<<std::endl;*/

    for(size_t i=1;i<=n_bits;i++){
        EXPECT_EQ(ss(i), select[i]);
    }

    //test the assignment operator
    SelectSupport<k, s> ss2 = ss;
    for(size_t i=1;i<=n_bits;i++){
        EXPECT_EQ(ss2(i), select[i]);
    }

    //test the copy constructor
    SelectSupport<k, s> ss3(ss2);
    for(size_t i=1;i<=n_bits;i++){
        EXPECT_EQ(ss3(i), select[i]);
    }

    //test the setBitVector function
    SelectSupport<k, s> ss4;
    ss4.setBitVector(&bv);
    for(size_t i=1;i<=n_bits;i++){
        EXPECT_EQ(ss4(i), select[i]);
    }

    //test the rank dictionary extracted from the select dictionary
    RankSupport<k, s> &rs = ss4.extractRankSupport();

    for(size_t i=0;i<bv.size();i++){
        EXPECT_EQ(rs(i), rank[i]);
    }

    delete[] select;
}

TEST(FixedLengthArraySuite, testRead){
    FixedWordArray<5> fqw(10);
    fqw[0] = 20;
    fqw[1] = 18;
    fqw[2] = 22;
    fqw[3] = 22;
    fqw[4] = 16;
    fqw[5] = 21;
    fqw[6] = 11;
    fqw[7] = 22;
    fqw[8] = 21;
    fqw[9] = 21;
    EXPECT_EQ(fqw[0], 20);
    EXPECT_EQ(fqw[1], 18);
    EXPECT_EQ(fqw[2], 22);
    EXPECT_EQ(fqw[3], 22);
    EXPECT_EQ(fqw[4], 16);
    EXPECT_EQ(fqw[5], 21);
    EXPECT_EQ(fqw[6], 11);
    EXPECT_EQ(fqw[7], 22);
    EXPECT_EQ(fqw[8], 21);
    EXPECT_EQ(fqw[9], 21);
    EXPECT_EQ(fqw.size(), 10);
    fqw.resize(20);
    EXPECT_EQ(fqw[0], 20);
    EXPECT_EQ(fqw[1], 18);
    EXPECT_EQ(fqw[2], 22);
    EXPECT_EQ(fqw[3], 22);
    EXPECT_EQ(fqw[4], 16);
    EXPECT_EQ(fqw[5], 21);
    EXPECT_EQ(fqw[6], 11);
    EXPECT_EQ(fqw[7], 22);
    EXPECT_EQ(fqw[8], 21);
    EXPECT_EQ(fqw[9], 21);
    EXPECT_EQ(fqw.size(), 20);
    fqw.resize(8);
    EXPECT_EQ(fqw[0], 20);
    EXPECT_EQ(fqw[1], 18);
    EXPECT_EQ(fqw[2], 22);
    EXPECT_EQ(fqw[3], 22);
    EXPECT_EQ(fqw[4], 16);
    EXPECT_EQ(fqw[5], 21);
    EXPECT_EQ(fqw[6], 11);
    EXPECT_EQ(fqw[7], 22);
    EXPECT_EQ(fqw.size(), 8);
    FixedWordArray<5> fwa = fqw;
    EXPECT_EQ(fwa[0], 20);
    EXPECT_EQ(fwa[1], 18);
    EXPECT_EQ(fwa[2], 22);
    EXPECT_EQ(fwa[3], 22);
    EXPECT_EQ(fwa[4], 16);
    EXPECT_EQ(fwa[5], 21);
    EXPECT_EQ(fwa[6], 11);
    EXPECT_EQ(fwa[7], 22);
    EXPECT_EQ(fwa.size(), fqw.size());

};

TEST(FixedLengthArraySuite, testWrite){
    PBitVector bv = {1,1,0,1,1,0,0,0,0,0,0,0,1,1};
    EXPECT_EQ(bv[0], true);
    EXPECT_EQ(bv[1], true);
    EXPECT_EQ(bv[2], false);
    EXPECT_EQ(bv[3], true);
    EXPECT_EQ(bv[4], true);
    EXPECT_EQ(bv[5], false);
    EXPECT_EQ(bv[6], false);
    EXPECT_EQ(bv[7], false);
    EXPECT_EQ(bv[8], false);
    EXPECT_EQ(bv[9], false);
    EXPECT_EQ(bv[10], false);
    EXPECT_EQ(bv[11], false);
    EXPECT_EQ(bv[12], true);
    EXPECT_EQ(bv[13], true);
    EXPECT_EQ(bv.size(), 14);
    PBitVector bv1;
    bv1 = bv;
    EXPECT_EQ(bv1[0], true);
    EXPECT_EQ(bv1[1], true);
    EXPECT_EQ(bv1[2], false);
    EXPECT_EQ(bv1[3], true);
    EXPECT_EQ(bv1[4], true);
    EXPECT_EQ(bv1[5], false);
    EXPECT_EQ(bv1[6], false);
    EXPECT_EQ(bv1[7], false);
    EXPECT_EQ(bv1[8], false);
    EXPECT_EQ(bv1[9], false);
    EXPECT_EQ(bv1[10], false);
    EXPECT_EQ(bv1[11], false);
    EXPECT_EQ(bv1[12], true);
    EXPECT_EQ(bv1[13], true);
    EXPECT_EQ(bv1.size(), 14);
    PBitVector bv2(bv1);
    EXPECT_EQ(bv2[0], true);
    EXPECT_EQ(bv2[1], true);
    EXPECT_EQ(bv2[2], false);
    EXPECT_EQ(bv2[3], true);
    EXPECT_EQ(bv2[4], true);
    EXPECT_EQ(bv2[5], false);
    EXPECT_EQ(bv2[6], false);
    EXPECT_EQ(bv2[7], false);
    EXPECT_EQ(bv2[8], false);
    EXPECT_EQ(bv2[9], false);
    EXPECT_EQ(bv2[10], false);
    EXPECT_EQ(bv2[11], false);
    EXPECT_EQ(bv2[12], true);
    EXPECT_EQ(bv2[13], true);
    EXPECT_EQ(bv2.size(), 14);
};

TEST(FixedLengthArraySuite, testRankSupport){
/*
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<size_t> distr(1, 1000000);

    for(size_t i=0;i<20;i++){
        rankSupportTester<2, 4>(distr(eng));
        rankSupportTester<4, 16>(distr(eng));
        rankSupportTester<8, 32>(distr(eng));
        rankSupportTester<16, 48>(distr(eng));
        rankSupportTester<32, 96>(distr(eng));
        rankSupportTester<64, 192>(distr(eng));
        rankSupportTester<128, 384>(distr(eng));
    }
    */
};

TEST(FixedLengthArraySuite, testSelectSupport){
/*
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<size_t> distr(1, 1000000);
    for(size_t i=0;i<=16;i++){
        selectSupportTester<64,4096>(distr(eng));
        selectSupportTester<128,16384>(distr(eng));
        selectSupportTester<192,36864>(distr(eng));
        selectSupportTester<192,36864>(distr(eng));
        selectSupportTester<256,65536>(distr(eng));
    }*/
};

TEST(FixedLengthArraySuite, testSparseSelectSupport){
    //TODO perform unit testing in SparseBV
    PBitVector sp_bv = {0,1,0,1,0,1,0,0,0,1,0,1,0,0,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,1,0,1,1,0,1,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1,0};
    SparseSelectSupport ssp(&sp_bv);

    for(size_t i=1;i<=sp_bv.nBits();i++){
        std::cout<<ssp(i)<<std::endl;
    }
};

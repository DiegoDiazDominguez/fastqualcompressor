//
// Created by Diego Diaz on 8/23/18.
//

#ifndef FASTQ_QUAL_COMPRESSION_WAVELETTREE_HPP
#define FASTQ_QUAL_COMPRESSION_WAVELETTREE_HPP
#include <vector>
#include <algorithm>
#include "CBitVector.hpp"
#include <sdsl/bit_vectors.hpp>
#include <sdsl/rrr_vector.hpp>

#include "HuffmanCodes.hpp"

template<class BVClass,
         class RSClass,
         class VClass>
class HuffWaveletTree {

private:
    struct Node{
        const bool is_leaf;
    protected:
        explicit Node(bool leaf) : is_leaf(leaf){}
    };

    struct IntNode: Node{
        sdsl::rrr_vector<63> bv;
        sdsl::rrr_vector<63>::rank_1_type rs;
        Node *left;
        Node *right;
        IntNode(): Node(false){}
    };

    struct Leaf: Node{
        unsigned char symbol;
        Leaf(): Node(true),
                symbol(0){}
    };

private:
    Node* build(VClass& sequence, bool is_leaf, size_t depth, std::map<size_t, std::pair<size_t, size_t>>& huff_codes){
        size_t zl=0, zr=0, bit_shift;
        bool left_child_is_leaf=true;
        bool right_child_is_leaf=true;

        if(is_leaf){
            auto *v = new Leaf();
            v->symbol = (unsigned char)sequence[0];
            return v;
        }

        sdsl::bit_vector tmp_bv;

        VClass sl;
        VClass sr;

        //check which symbols go to left and which one go to right
        std::map<size_t, bool> is_left;
        for(auto it=huff_codes.begin();it!=huff_codes.end();++it){
            bit_shift = (*it).second.second - depth -1;
            is_left[(*it).first] = ((*it).second.first & (1U << bit_shift))==0;
        }

        auto *v = new IntNode();

        for(size_t i=0;i<sequence.size();i++){
            if(is_left[sequence[i]]){
                zl++;
            }else{
                zr++;
            }
        }
        sl.resize(zl);
        sr.resize(zr);

        zl=0;
        zr=0;

        //v->bv.resize(sequence.size());
        tmp_bv.resize(sequence.size());

        for(size_t i=0;i<sequence.size();i++){
            if(is_left[sequence[i]]){
                tmp_bv[i] = false;
                sl[zl] = sequence[i];
                if(zl!=0 && sl[zl-1]!=sl[zl]){//check if the left size has more than one symbol
                    left_child_is_leaf = false;
                }
                zl++;
            }else{
                tmp_bv[i] = true;
                //v->bv[i] = true;
                sr[zr] = sequence[i];
                if(zr!=0 && sr[zr-1]!=sr[zr]){//check if the right side has more than one symbol
                    right_child_is_leaf = false;
                }
                zr++;
            }
        }

        //TODO is it necessary to call the destructor?
        v->left = build(sl, left_child_is_leaf, depth + 1, huff_codes);
        v->right = build(sr, right_child_is_leaf, depth + 1, huff_codes);

        //TODO add rank (and maybe select) support
        //TODO add rank support if the size of the vector is big (if is to short is better to perform popcounting)
        //v->rs.setBitVector(&v->bv);
        sdsl::rrr_vector<63> tmp_rrr_bv(tmp_bv);
        sdsl::rrr_vector<63>::rank_1_type rs(&tmp_rrr_bv);
        v->bv.swap(tmp_rrr_bv);
        v->rs.swap(rs);

        return v;
    }

    void free_tree(Node * root){
        if(!root->is_leaf){
            auto tmp = (IntNode *) root;
            free_tree(tmp->left);
            free_tree(tmp->right);
            delete(tmp->left);
            delete(tmp->right);
        }
    }

    void get_tree_size(Node * root, size_t *size){

        if(root->is_leaf){
            *size+=1;
        }else{
            auto tmp = (IntNode*)root;
            *size += sdsl::size_in_bytes(tmp->bv);
            *size += sdsl::size_in_bytes(tmp->rs);
            *size += sizeof(tmp->left);
            *size += sizeof(tmp->right);
            std::cout<<sizeof(tmp->right)<<std::endl;
            std::cout<<sizeof(tmp->left)<<std::endl;
            get_tree_size(tmp->left, size);
            get_tree_size(tmp->right, size);
        }
    };

private:
    size_t m_sigma, m_size;
    struct Node* wt_root;
    std::map<size_t, std::pair<size_t, size_t>> huff_codes_map;

private:
    std::vector<std::pair<size_t, size_t>> get_symbol_freqs(VClass v){

        std::map<size_t, size_t> symbols_map;
        std::vector<std::pair<size_t, size_t>> freqs_vector;

        //get the frequencies of the symbols
        for(size_t i=0;i<v.size();i++){
            symbols_map[v[i]]++;
        }

        freqs_vector.reserve(symbols_map.size());
        for (auto &it : symbols_map) {
            freqs_vector.emplace_back(it);
        }

        return freqs_vector;
    };

public:
    const size_t& size = m_size;
    const size_t& sigma = m_sigma;

public:
    explicit HuffWaveletTree(VClass v){

        std::vector<std::pair<size_t, size_t>> freqs_vector;
        std::vector<std::tuple<size_t, size_t, size_t>> huff_codes;

        freqs_vector = get_symbol_freqs(v);

        m_sigma = freqs_vector.size();
        m_size = v.size();

        //tuple is {symbol, code, code_length}
        huff_codes = HuffmanCodes::getHuffmanCodes(freqs_vector);

        for (auto &huff_code : huff_codes) {
            huff_codes_map[std::get<0>(huff_code)] = std::make_pair(std::get<1>(huff_code),
                                                                    std::get<2>(huff_code));
        }

        /*for(size_t i=0;i<freqs_vector.size();i++){
            std::cout<< (char)freqs_vector[i].first<<" "<<freqs_vector[i].second<<std::endl;
        }

        for (auto &huff_code : huff_codes) {
            std::cout<<(char)std::get<0>(huff_code)<<" "<<std::bitset<5>(std::get<1>(huff_code))<<" "<<std::get<2>(huff_code)<<std::endl;
        }*/

        wt_root = build(v, freqs_vector.size()<=1, 0, huff_codes_map);
    }

    HuffWaveletTree(): wt_root(nullptr),
                       m_sigma(0),
                       m_size(0){}

    ~HuffWaveletTree(){
        free_tree(wt_root);
    }

    void setVector(VClass& v){

        std::vector<std::pair<size_t, size_t>> freqs_vector;
        std::vector<std::tuple<size_t, size_t, size_t>> huff_codes;

        freqs_vector = get_symbol_freqs(v);
        m_sigma = freqs_vector.size();
        m_size = v.size();

        //tuple is {symbol, code, code_length}
        huff_codes = HuffmanCodes::getHuffmanCodes(freqs_vector);

        for (auto &huff_code : huff_codes) {
            huff_codes_map[std::get<0>(huff_code)] = std::make_pair(std::get<1>(huff_code),
                                                                    std::get<2>(huff_code));
        }

        wt_root = build(v, freqs_vector.size()<=1, 0, huff_codes_map);
    }

    uint8_t operator[](size_t index){
        return access(index);
    }

    uint8_t access(size_t index){
        //TODO assert that the symbol is in the alphabet
        assert(index<m_size);
        Node *node = wt_root;
        while(!node->is_leaf){
            auto *tmp = (IntNode *)node;
            if(tmp->bv[index]==0){
                index = index-tmp->rs(index);
                node = tmp->left;
            }else{
                index = tmp->rs(index)-1;
                node = tmp->right;
            }
        }
        return ((Leaf *)node)->symbol;
    }

    std::pair<uint8_t, size_t> rank_access(size_t index){
        //TODO assert that the symbol is in the alphabet
        assert(index<m_size);
        Node *node = wt_root;
        while(!node->is_leaf){
            auto *tmp = (IntNode *)node;
            if(tmp->bv[index]==0){
                index = index-tmp->rs(index);
                node = tmp->left;
            }else{
                index = tmp->rs(index)-1;
                node = tmp->right;
            }
        }
        return std::make_pair(((Leaf *)node)->symbol,
                              (index+1));
    }

    size_t rank(uint8_t symbol, size_t index){
        //TODO assert that the symbol is in the alphabet
        assert(index<m_size);
        Node *node = wt_root;
        std::pair<size_t, size_t> code = huff_codes_map[symbol];
        bool bit;
        size_t shift,i=0;

        while(!node->is_leaf){
            shift = (code.second-i-1);
            bit = (code.first & (1U<<shift))!=0;
            auto *tmp = (IntNode *)node;
            if(bit==0){
                index = index - tmp->rs(index);
                node = tmp->left;
            }else{
                index = tmp->rs(index)-1;
                node = tmp->right;
            }
            i++;
        }
        return (index+1);
    }

    size_t size_in_bytes(){
        size_t size =0;
        get_tree_size(wt_root, &size);
        size+=sizeof(m_sigma)+
              sizeof(m_size)+
              sizeof(wt_root);
        //TODO add the size of the huffman codes
        return size;
    }
};


#endif //FASTQ_QUAL_COMPRESSION_WAVELETTREE_HPP

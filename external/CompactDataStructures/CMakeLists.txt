cmake_minimum_required(VERSION 3.10)
project(SuccinctDataStructures CXX)

set(CMAKE_CXX_STANDARD 11)
cmake_minimum_required(VERSION 2.8)

set (CMAKE_CXX_FLAGS "-msse4.2 -Ofast -funroll-loops -fomit-frame-pointer -ffast-math -DNDEBUG")

add_library(sdts STATIC
        PBitVector.hpp
        FixedWordArray.hpp
        RankSupport.hpp
        SelectSupport.hpp
        SparseSelectSupport.hpp
        SparseSelectSupport.cpp CBitVector.cpp CBitVector.hpp)


#enable_testing()
#add_subdirectory(Tests)
//
// Created by diediaz on 27-07-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_SELECTSUPPORT_HPP
#define FASTQ_QUAL_COMPRESSION_SELECTSUPPORT_HPP

#include <algorithm>
#include "PBitVector.hpp"
#include "FixedWordArray.hpp"
#include "RankSupport.hpp"


class SelectSupport {
private:
    static const size_t W = (size_t)std::numeric_limits<size_t>::digits;
    const PBitVector *m_bv;
    FixedWordArray<> S;
    FixedWordArray<> I;
    PBitVector V;
    size_t l, s, n, m, k, words_per_block;

    RankSupport<> R;
    RankSupport<> R_V;

private:
    void populateVectors(){
        size_t S_size, r=0, w_raised;
        S_size = (size_t)ceil((double)m/s)+1;
        S.resize(S_size);

        //populate S
        for(size_t i=0;i< m_bv->size();i++){
            if((*m_bv)[i]){
                r++;
                if((r % s)==1){
                    S[(r-1)/s] = i;
                }
            }
        }

        S[S_size-1] = m_bv->size();

        //populate V and I
        V.resize((size_t)std::max(1,(int)S.size()-1));
        w_raised = k*k;
        I.resize((size_t)ceil((double)n/(s*w_raised)) * s);

        size_t k=0;
        for(size_t i=0;i<S.size()-1;i++){
            if((S[i+1]-S[i])<=s*w_raised){
                V[i] = false;
            }else{
                V[i] = true;
                for(size_t j=S[i];j<S[i+1];j++){
                    if((*m_bv)[j]){
                        I[k] = j - S[i];
                        k++;
                    }
                }
            }
        }
        I.resize((size_t) std::max(1, (int)k));
        R_V.setBitVector(&V);
    };

    size_t binarySearch(size_t start, size_t end, size_t value, FixedWordArray<>& arr){
        size_t mid, tmp, pos=0;
        assert(start<=end);
        mid = start + (size_t)ceil((double)(end-start)/2);
        tmp = arr[mid];
        if(tmp==value) return mid-1;

        if (tmp > value) {
            pos = binarySearch(start, mid-1, value, arr);
        } else if (tmp < value){
            if(mid+1==arr.size()) return mid; //border case
            tmp = arr[mid+1];
            if(tmp==0|| tmp >=value){ //tmp==0 means superblock ending
                pos = mid;
            } else {
                pos = binarySearch(mid, end, value, arr);
            }
        }
        return pos;
    };

    void copy(const SelectSupport &other){
        m_bv = other.m_bv;
        S = other.S;
        I = other.I;
        V = other.V;
        n = other.n;
        m = other.m;
        l = other.l;
        s = other.s;
        k = other.k;
        words_per_block = other.words_per_block;
        R = other.R;
        R_V = other.R_V;
    };

    bool is_power_of_two(unsigned long x){
        return (x & (x - 1)) == 0;
    };

    unsigned long upper_power_of_two(unsigned long v){

        if(v==0 | v==1) return 2;
        if(is_power_of_two(v)) return v;

        v--;
        v |= v >> 1UL;
        v |= v >> 2UL;
        v |= v >> 4UL;
        v |= v >> 8UL;
        v |= v >> 16UL;
        v++;
        return v;
    };

public:
    explicit SelectSupport(PBitVector *bv){
        m_bv = bv;
        n = m_bv->size();
        m = m_bv->nBits();

        l = upper_power_of_two((size_t)ceil(log2(n)*log2(log2(n))));
        s = upper_power_of_two((size_t)ceil(pow(log2(n),2)*log2(log2(n))));
        m_bv = bv;
        k = s/l;
        words_per_block = l/W;
        R.setSamplingRates(l,s);

        R.setBitVector(m_bv);
        populateVectors();
    };

    SelectSupport(){
        m_bv = nullptr;
        l = 0;
        s = 0;
        k = 0;
        n = 0;
        m = 0;
        words_per_block = l/W;
    };

    SelectSupport(const SelectSupport &other){
        copy(other);
    };

    size_t operator()(size_t rank){
        size_t p, is, ie, iw, r;
        if(rank>m) return n;
        if(rank==0) return 0;

        p =(size_t)ceil((double)rank/s) - 1;

        if(V[p]){
            return S[p] + I[s*(R_V(p)-1) + ((rank-1)%s)];
        }else{
            is = S[p]/s;
            ie = (S[p+1]-1)/s;

            //binary search in R
            is = binarySearch(is, ie, rank, R.R);
            r = R.R[is];

            ie = std::min(R.r.size()-1, (is+1)*k);
            is = is*k;

            //binary search in r
            is = binarySearch(is, ie,rank-r,R.r);
            r += R.r[is];

            iw = is * words_per_block;

            //scan over last block l
            while((r+__builtin_popcountll(m_bv->m_W[iw])) < rank){
                r += __builtin_popcountll(m_bv->m_W[iw]);
                iw++;
            }

            is = iw<<LOG_W;
            while(r!=rank){
                if((*m_bv)[is]){
                    r++;
                }
                is++;
            }
            return (is-1);
        }
    };

    SelectSupport& operator=(const SelectSupport& other){
        copy(other);
        return *this;
    };

    void setBitVector(const PBitVector *bv){
        m_bv = bv;
        n = (*m_bv).size();
        m = (*m_bv).nBits();
        //TODO set the mult by template argument
        size_t test = W*4;
        /*l = upper_power_of_two((size_t)ceil(log2(n)*log2(log2(n))));
        s = upper_power_of_two((size_t)ceil(pow(log2(n),2)*log2(log2(n))));
        m_k = s/l;*/
        l = upper_power_of_two((size_t)ceil(test*log2(test)));
        s = upper_power_of_two((size_t)ceil(pow(test,2)*log2(test)));
        k = s/l;
        words_per_block = l/W;
        R.setSamplingRates(l,s);
        R.setBitVector(m_bv);
        populateVectors();
    };

    RankSupport<>& extractRankSupport(){
        return R;
    };

    size_t size_in_bytes() const{
        //std::cout<<l<<" jojoj"<<s<<std::endl;
        //std::cout<<"S: "<<S.size_in_bytes()<<std::endl;
        //std::cout<<"I: "<<I.size_in_bytes()<<std::endl;
        //std::cout<<"V: "<<V.size_in_bytes()<<std::endl;
        //std::cout<<"R: "<<R.size_in_bytes()<<std::endl;
        //std::cout<<"R_V: "<<R_V.size_in_bytes()<<std::endl;

        return sizeof(size_t)*6 +
               sizeof(m_bv) +
               S.size_in_bytes()+
               I.size_in_bytes()+
               V.size_in_bytes()+
               R.size_in_bytes()+
               R_V.size_in_bytes();
    };
};


#endif //FASTQ_QUAL_COMPRESSION_SELECTSUPPORT_HPP

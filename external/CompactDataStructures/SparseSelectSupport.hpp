//
// Created by diediaz on 02-08-18.
//

#ifndef SUCCINCTDATASTRUCTURES_SPARSEBVSELECT_HPP
#define SUCCINCTDATASTRUCTURES_SPARSEBVSELECT_HPP

#include "PBitVector.hpp"
#include "FixedWordArray.hpp"
#include "SelectSupport.hpp"
#include "CBitVector.hpp"

class SparseSelectSupport {
private:
    size_t r, n, m;
    FixedWordArray<> LO;
    PBitVector H;
    //TODO set l and m_s with respect to n
    SelectSupport hs;

private:
    void populateVectors(const PBitVector* sparse_bv){
        size_t rank=0, shift;
        FixedWordArray<> S(m);

        //fill S
        for(size_t i=0;i<n;i++){
            if((*sparse_bv)[i]){
                S[rank] = i;
                rank++;
            }
        }

        //populate L and H
        shift = 1ULL<<r;
        if(r!=0){
            LO.resize(m);
        }
        H.resize((m-1) + (S[m-1]>>r) + 1);

        for(size_t i=0;i<S.size();i++){
            size_t tmp = i + (S[i]>>r);
            if(r!=0){
                LO[i] = S[i] & (shift - 1); //r lowest bits
            }
            H[tmp] = true;
        }

        hs.setBitVector(&H);
    };
public:
    explicit SparseSelectSupport(const PBitVector* sparse_bv){
        n = sparse_bv->size();
        m = sparse_bv->nBits();
        r = (size_t)floor(log2((double)n/m));
        LO.setWidth(r);
        populateVectors(sparse_bv);
    };

    SparseSelectSupport(){
        r = 0;
        n = 0;
        m = 0;
    };

    void setBitVector(const PBitVector* sparse_bv){
        n = sparse_bv->size();
        m = sparse_bv->nBits();
        r = (size_t)floor(log2((double)n/m));
        LO.setWidth(r);
        populateVectors(sparse_bv);
    }

    size_t operator()(size_t rank){
        assert(rank>0 && rank<=m);
        return (((hs(rank)+1)-rank)<<r) + LO[rank-1];
    }

    size_t size_in_bytes(){

        size_t tot_size = LO.size_in_bytes()+
                          H.size_in_bytes()+
                          hs.size_in_bytes();

        /*std::cout<<"\nm: "<<m<<" n:"<<n<<" r:"<<r<<std::endl;
        std::cout<<"LO: "<<LO.size_in_bytes()<<" "<<(double)LO.size_in_bytes()/tot_size<<std::endl;
        std::cout<<"H: "<<H.size_in_bytes()<<" "<<(double)H.size_in_bytes()/tot_size<<" "<<H.occupancy()<<std::endl;
        std::cout<<"hs: "<<hs.size_in_bytes()<<" "<<(double)hs.size_in_bytes()/tot_size<<std::endl;*/

        return sizeof(size_t)*3+
              LO.size_in_bytes()+
              H.size_in_bytes()+
              hs.size_in_bytes();
    };
};


#endif //SUCCINCTDATASTRUCTURES_SPARSEBVSELECT_HPP

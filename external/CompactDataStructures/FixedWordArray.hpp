//
// Created by diediaz on 23-07-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_FIXEDWORDARRAY_HPP
#define FASTQ_QUAL_COMPRESSION_FIXEDWORDARRAY_HPP
#include <iostream>
#include <limits>
#include <cstring>
#include <cassert>
#include <cmath>

template<size_t word_size= (size_t) std::numeric_limits<size_t>::digits>
class FixedWordArray {
private:
    unsigned long long *W;
    const static size_t m_word_real_size = (size_t)std::numeric_limits<size_t>::digits;
    size_t m_virtual_len;
    size_t m_real_len;
    size_t m_word_width;

private:
    inline size_t elmRead(size_t index) const{
        assert(index < m_virtual_len);
        size_t i,j, cell_i, cell_j;
        i = index*m_word_width;
        j = (index+1)*m_word_width-1;
        cell_i = i/m_word_real_size;
        cell_j = j/m_word_real_size;

        if(m_word_width==m_word_real_size){ //border case
            return W[cell_j];
        }else if(cell_i == cell_j){
            return (W[cell_j] >> (i & (m_word_real_size-1))) & ((1ULL<<(j-i+1))-1);
        }else{
            return (W[cell_i] >> (i & (m_word_real_size-1) )  |
                    (W[cell_j] & ((1ULL <<((j+1) & (m_word_real_size-1)))-1)) << (m_word_real_size - (i & (m_word_real_size-1))));
        }
    };

    inline void elmWrite(size_t index, size_t value){
        assert(index < m_virtual_len);
        size_t i,j, cell_i, cell_j;
        i = index*m_word_width;
        j = (index+1)*m_word_width-1;
        cell_i = i/m_word_real_size;
        cell_j = j/m_word_real_size;

        if(m_word_width == m_word_real_size){// border case
            W[cell_j] = value;
        }else if(cell_i==cell_j){
            W[cell_j] &= ~(((1ULL<<(j-i+1)) -1) << (i & m_word_real_size-1));
            W[cell_j] |= value << (i & m_word_real_size-1);
        }else{
            W[cell_i] = (W[cell_i] & ((1ULL<<(i & (m_word_real_size-1)))-1)) | (value << (i & (m_word_real_size-1)));
            W[cell_j] = (W[cell_j] & ~((1ULL<<((j+1) & (m_word_real_size-1)))-1)) |
                        (value >> (m_word_real_size-(i & (m_word_real_size-1))));
        }
    };

    void copy(const FixedWordArray& other){
        m_real_len = other.m_real_len;
        m_virtual_len = other.m_virtual_len;
        m_word_width = other.m_word_width;
        if(m_real_len != 0){
            if(W!=nullptr){
                free(W);
            }
            W = new unsigned long long int[m_real_len];
            std::memcpy(W, other.W, m_real_len*sizeof(unsigned long long int));
        }
    };

public:
    class Proxy{
       friend class FixedWordArray;
    private:
        FixedWordArray & m_fwa;
        size_t m_index;
        Proxy(FixedWordArray & customArray, size_t index): m_fwa(customArray),
                                                           m_index(index){}
    public:
        inline operator size_t()const{
            return m_fwa.elmRead(m_index);
        }

        inline Proxy& operator=(size_t value){
            m_fwa.elmWrite(m_index, value);
            return *this;
        }
    };
public:
    explicit FixedWordArray(size_t array_size){
        m_word_width = word_size;
        assert(word_size<=m_word_real_size);
        m_virtual_len = array_size;
        m_real_len = (size_t)ceil((double)(m_virtual_len*m_word_width)/m_word_real_size);
        W = new unsigned long long[m_real_len];
    };

    ~FixedWordArray(){
        delete[] W;
    };

    FixedWordArray(const FixedWordArray& other){
        W = nullptr;
        copy(other);
    };

    FixedWordArray(){
        m_real_len = 0;
        m_virtual_len = 0;
        m_word_width = word_size;
        W= nullptr;
    };

    void flush(){
        if(W!=nullptr){
            memset(W, 0, m_real_len*sizeof(unsigned long long));
        }
    };

    void resize(size_t new_size){
        m_virtual_len = new_size;
        if(new_size==0) {
            m_real_len = 0;
            if (W != nullptr) {
                delete[] W;
            }
            W = nullptr;
        }else {
            auto tmp = (size_t) ceil((double) (m_virtual_len * m_word_width) / m_word_real_size);
            if (tmp != m_real_len) {
                m_real_len = tmp;
                auto *tmp_ptr = (unsigned long long *) realloc(W, m_real_len * sizeof(unsigned long long));
                if (tmp_ptr != nullptr) {
                    if (W == nullptr) {
                        memset(tmp_ptr, 0, m_real_len * sizeof(unsigned long long));
                    }
                    W = tmp_ptr;
                } else {
                    std::cout << "Error trying to resize the array" << std::endl;
                    exit(EXIT_FAILURE);
                }
            }
        }
    };

    inline size_t size() const {
        return m_virtual_len;
    };

    void setWidth(size_t new_width){

        m_word_width = new_width;
        if(W!=nullptr){
            auto *tmp = new unsigned long long int[m_virtual_len];
            for(size_t i=0;i<m_virtual_len;i++){
                tmp[i] = elmRead(i);
            }
            memset(W, 0, m_real_len*sizeof(unsigned long long int));
            for(size_t i=0;i<m_virtual_len;i++){
                elmWrite(i, tmp[i]);
            }
            delete[] tmp;
        }
    };

    inline size_t operator[](size_t index) const {
        return elmRead(index);
    }

    inline Proxy operator[](size_t index){
        return Proxy(*this, index);
    }

    FixedWordArray<word_size>& operator=(const FixedWordArray& other){
        copy(other);
        return *this;
    };

    size_t size_in_bytes() const{
        return sizeof(size_t)*4+
               sizeof(unsigned long long)*m_real_len+
               sizeof(W);
    };

    size_t getWordWidth() const{
        return m_word_width;
    }
};

#endif //FASTQ_QUAL_COMPRESSION_FIXEDWORDARRAY_HPP

//
// Created by diediaz on 24-08-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_HUFFMANCODES_HPP
#define FASTQ_QUAL_COMPRESSION_HUFFMANCODES_HPP

#include <iostream>
#include <algorithm>
#include <map>
#include <vector>
#include <stack>

class HuffmanCodes {

    struct HuffmanTreeNode{
        size_t symbol;
        size_t freq;
        HuffmanTreeNode *left;
        HuffmanTreeNode *right;
        HuffmanTreeNode *next;
    };

    static HuffmanTreeNode* getHuffmanTree(std::vector<std::pair<size_t, size_t>>& freqs_vector){

        std::sort(freqs_vector.begin(), freqs_vector.end(),[](const std::pair<size_t, size_t> & a,
                                                              const std::pair<size_t, size_t> & b) -> bool{
            return a.second < b.second;
        });

        auto *head = new HuffmanTreeNode();
        auto *node = head;

        for (size_t i=0;i<freqs_vector.size();i++) {
            node->symbol = freqs_vector[i].first;
            node->freq = freqs_vector[i].second;
            node->left = nullptr;
            node->right = nullptr;

            //allocate for the next node
            if(i!=(freqs_vector.size()-1)){
                //next node
                node->next = new HuffmanTreeNode();
                node = node->next;
                node->next = nullptr;
            }
        }

        node = head;

        while(head->next != nullptr){
            auto *h_node = new HuffmanTreeNode();

            h_node->left = head;
            h_node->right = head->next;
            h_node->freq = h_node->left->freq + h_node->right->freq;

            while(node->next != nullptr &&
                  node->next->freq <= h_node->freq){
                node = node->next;
            }

            h_node->next = node->next;
            node->next = h_node;
            head = head->next->next;
        };
        return head;
    }

    static void getHuffmanCodesInt(std::vector<std::tuple<size_t, size_t, size_t>> * S,
                                   HuffmanTreeNode *node, size_t depth, size_t code){

        if(node->left==nullptr){
            (*S).emplace_back(node->symbol, code, depth);
        }else{
            getHuffmanCodesInt(S, node->left, depth+1, (code<<1ULL));
            getHuffmanCodesInt(S, node->right, depth+1, (code<<1ULL)|1);
        }
    }

public:
    static std::vector<std::tuple<size_t, size_t, size_t>> getHuffmanCodes(std::vector<std::pair<size_t, size_t>>& symbols_freqs){

        //tuple is {symbol, code, code_length}
        std::vector<std::tuple<size_t, size_t, size_t>> codes;
        HuffmanTreeNode *root = getHuffmanTree(symbols_freqs);
        getHuffmanCodesInt(&codes, root, 0,0);
        return codes;
    };
};


#endif //FASTQ_QUAL_COMPRESSION_HUFFMANCODES_HPP

//
// Created by diediaz on 23-08-18.
//

#ifndef FASTQ_QUAL_COMPRESSION_PARTITIONEDCYCLE_HPP
#define FASTQ_QUAL_COMPRESSION_PARTITIONEDCYCLE_HPP

#include "Cycle.hpp"

class PartitionedCycle: public Cycle {

    friend class CompFQualArray;

private:
    sdsl::wt_huff<sdsl::rrr_vector<63>> m_p_classes_wt;
    std::vector<FixedWordArray<>> m_p_quals;

    uint8_t m_n_classes=0;

public:

    PartitionedCycle(uint8_t n_classes, std::vector<unsigned char>& quals_map, size_t n_symbols){
        m_n_classes = n_classes;
        m_rank2qual = quals_map;

        m_p_quals.resize(size_t(n_classes));
        for(size_t j=0;j<n_classes;j++) {
            m_p_quals[j].setWidth(j + 1);
            m_p_quals[j].resize(n_symbols);
        }
    }

    size_t size_in_bytes() override {
        size_t tot_size=0;
        tot_size+=sdsl::size_in_bytes(m_p_classes_wt);

        for (auto &m_p_qual : m_p_quals) {
            tot_size+= m_p_qual.size_in_bytes();
        }

        tot_size+=m_rank2qual.size()+sizeof(m_n_classes);
        return tot_size;
    }

    double size_diff() override{
        double H0=0;
        size_t n_symbols=0;

        for(size_t i=0;i<m_p_classes_wt.sigma;i++){
            n_symbols=m_p_classes_wt.rank(m_p_classes_wt.size()-1, (unsigned char)i);
            H0 += double(n_symbols)/m_p_classes_wt.size() * log2(double(m_p_classes_wt.size())/n_symbols);
        }
        return sdsl::size_in_bytes(m_p_classes_wt)-((m_p_classes_wt.size()*H0)/8);
    }

    void build_array(std::string& buffer_file) override {
        sdsl::construct(m_p_classes_wt, buffer_file);
    }

    unsigned char operator[](size_t index) override{
        uint8_t qual_class = m_p_classes_wt[index];
        size_t value, class_rank;

        if(qual_class==0){
           value = 1;
        }else{
            class_rank = m_p_classes_wt.rank((index+1), qual_class);
            value = m_p_quals[qual_class-1][class_rank-1];
            value +=(1UL <<qual_class);
        }
        return m_rank2qual[value-1];
    };
};


#endif //FASTQ_QUAL_COMPRESSION_PARTITIONEDCYCLE_HPP

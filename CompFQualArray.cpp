//
// Created by diediaz on 23-07-18.
//

#include "CompFQualArray.hpp"
#include <algorithm>

CompFQualArray::CompFQualArray(std::string& file, unsigned char qual_threshold) {
    m_input_file = file;
    m_qual_threshold = qual_threshold;
    getStatsPerCycle();
    compressQuals();
}

void CompFQualArray::compressQuals(){


    size_t code, l, n_partitioned;
    FastQReader fqr(m_input_file, FastQReader::FORWARD);
    unsigned char curr_qual;
    int pos;

    n_partitioned = cycle_is_partitioned.nBits();

    std::vector<std::vector<size_t>> par_pos(n_partitioned, std::vector<size_t>(5, 0));

    //temporal bitmaps to create the huff-shaped wavelet trees of WT and AP
    sdsl::int_vector_buffer<8> classes_array[n_partitioned];
    std::vector<size_t> classes_pos(n_partitioned,0);

    sdsl::int_vector_buffer<8> quals_array[n_cycles-n_partitioned];
    std::vector<size_t> quals_pos(n_cycles-n_partitioned,0);

    for(size_t i=0;i<n_partitioned;i++){
        classes_array[i] = sdsl::int_vector_buffer<8>("classes_file_"+std::to_string(i), std::ios::out, 1000000, 8);
    }

    for(size_t i=0;i<(n_cycles-n_partitioned);i++){
        quals_array[i] = sdsl::int_vector_buffer<8>("quals_file_"+std::to_string(i), std::ios::out, 1000000, 8);
    }

    //create a temporal hash to map qualities to indexes
    std::vector<std::vector<size_t>> qual2rank(n_cycles, std::vector<size_t>(255));
    for(size_t i=0;i<n_cycles;i++){
        for(size_t j=0;j<cycles[i]->m_rank2qual.size();j++){
           qual2rank[i][cycles[i]->m_rank2qual[j]] = (j+1);
        }
    }

    while(!fqr.isFinished()){

        curr_qual = std::min(fqr.curr_symbol_qual, m_qual_threshold);
        code = qual2rank[fqr.curr_pos][curr_qual];
        if(!cycle_is_partitioned[fqr.curr_pos]){ //encoded cycle encoded as WT
            pos = int(fqr.curr_pos-cycle_is_partitioned.popCount(0, (size_t)fqr.curr_pos));
            quals_array[pos][quals_pos[pos]] = code;
            quals_pos[pos]++;

        }else{//cycle encoded as AP
            pos = std::max(0, int(cycle_is_partitioned.popCount(0, (size_t)fqr.curr_pos))-1);
            l = size_t(floor(log2(code)));
            classes_array[pos][classes_pos[pos]] = l;
            classes_pos[pos]++;

            if (l > 0) {
                dynamic_cast<PartitionedCycle*>(cycles[fqr.curr_pos])->m_p_quals[l - 1][par_pos[pos][l - 1]] = code - size_t(pow(2, l));
                par_pos[pos][l - 1]++;
            }
        }
        fqr.nextQualInRead();
    }

    //resize vectors
    for(size_t i=0;i<n_cycles;i++){
        if(cycle_is_partitioned[i]) {
            pos = std::max(0, int(cycle_is_partitioned.popCount(0, i))-1);
            auto pc = dynamic_cast<PartitionedCycle *>(cycles[i]);
            for(size_t j=0;j<pc->m_n_classes;j++) {
                pc->m_p_quals[j].resize(par_pos[pos][j]);
            }
        }
    }

    for(size_t i=0;i<n_cycles;i++){
        std::string tmp_file;
        if(cycle_is_partitioned[i]){
            pos = std::max(0, int(cycle_is_partitioned.popCount(0, i))-1);
            tmp_file = "classes_file_"+std::to_string(pos);
            classes_array[pos].close(false);
        }else{
            pos = int(i-cycle_is_partitioned.popCount(0, i));
            tmp_file = "quals_file_"+std::to_string(pos);
            quals_array[pos].close(false);
        }
        cycles[i]->build_array(tmp_file);
        std::remove(tmp_file.c_str());
    }
}

unsigned char CompFQualArray::decodeValue(size_t index) {
    assert(index <m_n_quals);
    size_t c_index = index%n_cycles;
    return (*cycles[c_index])[index/n_reads];
}

size_t CompFQualArray::size_in_bytes() {

    size_t tot_size=0;

    tot_size+=cycle_is_partitioned.size_in_bytes();
    for (auto &cycle : cycles) {
        tot_size+= cycle->size_in_bytes();
    }
    return tot_size;
}

size_t CompFQualArray::size() {
    return m_n_quals;
}


void CompFQualArray::getStatsPerCycle() {

    double  n_bits_PA=0, n_bits_regular=0;
    unsigned char curr_symbol;
    FastQReader fqr(m_input_file, FastQReader::FORWARD);
    if(fqr.PHRED33){
       m_qual_threshold+=33;
    }

    n_cycles = (size_t)fqr.curr_read_len;
    m_n_quals = fqr.n_chars;
    n_reads = fqr.n_reads;

    std::vector<std::vector<std::pair<unsigned char, size_t>>> freqs_per_cycle(n_cycles,
                                                                               std::vector<std::pair<unsigned char, size_t>>(255));

    cycles.resize(n_cycles);

    cycle_is_partitioned.resize(n_cycles);

    //initialize freqs array
    for (auto &i : freqs_per_cycle) {
        for(size_t j=0;j<255;j++){
           i[j] = std::make_pair(j,0);
        }
    }

    //get qual statistics per cycle
    while(!fqr.isFinished()){
        curr_symbol = std::min(fqr.curr_symbol_qual, m_qual_threshold);
        freqs_per_cycle[fqr.curr_pos][curr_symbol].second++;
        fqr.nextQualInRead();
    }

    //sort qualities per cycle (from the most abundant to the least abundant)
    for (auto &i : freqs_per_cycle) {

        std::sort(i.begin(),
                  i.end(),
                  [](const std::pair<unsigned char, size_t> & l, const std::pair<unsigned char, size_t> & r) -> bool
                  {
                      return l.second > r.second;
                  });

        size_t non_zero=0;
        for(auto &j :i){
            if(j.second!=0){
                non_zero++;
            }
        }
        i.resize(non_zero);
    }

    //check if is better to use regular WT or AP in every cycle
    for (size_t i=0;i<freqs_per_cycle.size();i++) {

        n_bits_PA = getAPspace(freqs_per_cycle[i]);
        n_bits_regular = getWTspace(freqs_per_cycle[i]);

        cycle_is_partitioned[i] = n_bits_PA<n_bits_regular;

        std::vector<unsigned char> tmp_qual_map;
        for(auto &freq : freqs_per_cycle[i]){
            tmp_qual_map.push_back(freq.first);
        }

        if(cycle_is_partitioned[i]){
            auto n_c = uint8_t(floor(log2(freqs_per_cycle[i].size())));
            cycles[i] = new PartitionedCycle(n_c, tmp_qual_map, fqr.n_reads);
        }else{
            cycles[i] = new RegularCycle(tmp_qual_map);
        }
    }
}

double CompFQualArray::getAPspace(std::vector<std::pair<unsigned char, size_t>> freqs) {

    size_t start, end, j, n_bits=0, n_elements=0;
    auto u = size_t(floor(log2(freqs.size())));
    std::vector<size_t> classes_freqs(u+1,0);
    double h0_entropy=0;

    j=2;
    for(size_t l=1;l<=u;l++){
        start = size_t(pow(2,l))-1;
        end = std::min(freqs.size()-1, size_t(pow(2,l+1)-1)-1);
        for(size_t k=start;k<=end;k++){
            n_bits+= freqs[k].second*l;
            j++;
        }

        if(end==(freqs.size()-1)){
            break;
        }
    }

    for (auto& n : freqs)
        n_elements += n.second;

    for(size_t i=1;i<freqs.size();i++){
        classes_freqs[size_t(floor(log2(i)))] += freqs[i].second;
    }

    for (unsigned long classes_freq : classes_freqs) {
        h0_entropy += (double(classes_freq)/n_elements) * log2(double(n_elements)/ classes_freq);
    }

    return n_bits + (n_elements*(h0_entropy+1));
}

double CompFQualArray::getWTspace(std::vector<std::pair<unsigned char, size_t>> freqs) {

    size_t n_elements=0;
    double h0_entropy=0;

    for (auto &freq : freqs) {
        n_elements+= freq.second;
    }

    for (auto &freq : freqs) {
        h0_entropy+= (double(freq.second)/n_elements) * log2(n_elements/freq.second);
    }

    return n_elements*(h0_entropy+1);
}


//
// Created by Diego Diaz on 6/1/18.
//

#ifndef STRINGFINGERPRINTGENERATOR_FASTQREADER_HPP
#define STRINGFINGERPRINTGENERATOR_FASTQREADER_HPP
extern "C"{
#include "kseq.h"
#include <zlib.h>
#include <cstdio>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
};
#include <iostream>
#include "DNAAlphabet.hpp"


class FastQReader {

KSEQ_INIT(gzFile, gzread);

private:
    std::string                         m_input_file; // input file
    int                                 m_curr_read;  // ID of the current read
    size_t                              m_printed_chars; // number of printed chars so far
    size_t                              m_n_chars;       // total number of DNA chars in the file
    size_t                              m_n_reads;       // total number of reads in the file
    int                                 m_curr_pos;      // current position (relative to the current read)
    int                                 m_curr_read_len; // length of the current read
    unsigned char                       m_curr_symbol_qual;   // current symbol (relative to the current read)
    bool                                m_direction;     // the way symbols in the reads are being read
    kseq_t                              *seq=nullptr;    // kseq objects
    gzFile                              fp;

public:
    const static int                    FORWARD=true;
    const static int                    BACKWARD=false;
    const bool &direction           =   m_direction;
    const size_t &n_chars           =   m_n_chars;
    const size_t &n_reads           =   m_n_reads;
    const int &curr_read_len        =   m_curr_read_len;
    const int &curr_pos             =   m_curr_pos;
    const unsigned char &curr_symbol_qual=   m_curr_symbol_qual;
    const std::string *input_file   =   &m_input_file;
    bool                                PHRED33=false;

private:
    void getFileStats();
    inline void nextRead(){
        m_curr_read_len  = kseq_read(seq);
        if(m_curr_read_len>0){
            ++m_curr_read;
        }
        if (m_direction) {
            m_curr_pos = -1;
        }else {
            m_curr_pos = m_curr_read_len;
        }
        nextQualInRead();
    };
    void initReader();
    void destroy();

public:
    FastQReader(std::string &file, bool dir);
    ~FastQReader(){
        destroy();
    };
    inline void nextQualInRead(){
        if(m_direction){
            if (m_curr_pos == m_curr_read_len-1) {
                nextRead();
                return;
            }else {
                ++m_curr_pos;
            }
        } else {
            if(m_curr_pos ==0){
                nextRead();
                return;
            }else {
                --m_curr_pos;
            }
        }
        m_curr_symbol_qual =  (unsigned char)seq->qual.s[m_curr_pos];
        ++m_printed_chars;
    };
    inline bool isFinished(){
        return m_printed_chars>m_n_chars;
    };
};


#endif //STRINGFINGERPRINTGENERATOR_FASTQREADER_HPP
